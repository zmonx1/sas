<?php defined('BASEPATH') or exit('No direct script access allowed');

class BD_Controller extends CI_Controller
{
    protected $userid = '';
    protected $username = '';
    protected $role = '';
    protected $global = [];
    public function isLoggedIn()
    {
        
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        $this->userid = $this->session->userdata('id');
        $this->username = $this->session->userdata('username');
        $this->role = $this->session->userdata('role');
        if (!isset($isLoggedIn) || $isLoggedIn != true) {
            redirect(base_url('login'));
        } else {
            $this->userid = $this->session->userdata('id');
            $this->username = $this->session->userdata('username');
        }
    }

    public function response($data = NULL, $http_code = NULL)
    {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    public function dd($data)
    {
        echo "<pre>" ;
        print_r($data);
        echo "</pre>";
        exit;

    }


}