<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfessorManagement extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		// $this->load->library('Pagination_bootstrap');
		$this->load->model('user_model');
		$this->load->model('course_model');
    }
	
	public function index()
	{
		$professor = $this->user_model->get_Professor();
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$data['professor'] = $professor;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('professorManagement/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}

	public function updateProfessor()
    {
		$proEmailCheck = $this->input->post('proEmailCheck');
		$prefixPro = $this->input->post('prefixPro');
        $firstNamePro = $this->input->post('firstNamePro');
		$lastNamePro = $this->input->post('lastNamePro');
		$emailPro = $this->input->post('emailPro');
		

		$data = array(
			"prefix" => $prefixPro,
			"firstName" => $firstNamePro,
			"lastName" => $lastNamePro,
			"email" => $emailPro
		);
		$this->user_model->updateProfessor($data,$proEmailCheck);   
        redirect(base_url('professorManagement'));
	
	}

	public function deleteProfessor()
    {	
		$proPrefix= $this->input->post('prefix');
		$proFirstName = $this->input->post('firstName');
		$proLastName = $this->input->post('lastName');
		$proEmail = $this->input->post('email');
		$proPass = $this->input->post('password');
		$proRole = $this->input->post('role');
		$proUserID = $this->input->post('userId');
		$data = array(
			"prefix" => $proPrefix,
			"firstName" => $proFirstName,
			"lastName" => $proLastName,
			"email" => $proEmail,
			"password" => $proPass,
			"role" => $proRole,
			"userId" => $proUserID,
		);
		$courseOffered = [];
		$course = $this->course_model->findAllCourseOffered();
		for ($i=0; $i < sizeOf($course); $i++) { 
			$courseOffered[$i] = $this->course_model->get_courseOffered($course[$i]['courseID']);
			for ($j=0; $j < sizeOf($courseOffered[$i][0]['teacherId']); $j++) { 
				if($proUserID === $courseOffered[$i][0]['teacherId'][$j]){
					unset($courseOffered[$i][0]['teacherId'][$j]);
					// $courseOffered = array_values($courseOffered[$i][0]['teacherId']);

					$this->user_model->delProEnroll($courseOffered[$i][0]['teacherId'],$courseOffered[$i][0]['courseID']);
				}
			}
		}
        $this->user_model->delProfessor($data);
		redirect(base_url('ProfessorManagement'));
    }
	public function getRandomHex($num_bytes=4) {
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function insertProfessor()
    {
		$pPrefix = $this->input->post('proPrefix-add-modal');
        $pFirstName = $this->input->post('pFirstName-add-modal');
		$pLastName = $this->input->post('pLastName-add-modal');
        $pEmail = $this->input->post('pEmail-add-modal');
		$pPassword = "Swe_001";
		$pRole = "3";
		$userId = $this->getRandomHex(8);

        $user = $this->user_model->get_user($pEmail);?>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (อาจารย์)</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body prompt">
					<p id="proNameShow2"></p>
					</div>
					<div class="modal-footer prompt">
						<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
					</div>
				</div>
			</div>
		</div>
	<?php
		if(empty($user)){
			$data1 = array(
				"prefix" => $pPrefix,
				"firstName" => $pFirstName,
				"lastName" => $pLastName,
				"email" => $pEmail,
				"password" => $pPassword,
				"role" => $pRole,
				"userId" => $userId,
			);
			$this->user_model->insertAlluser($data1);
			?>
			<script type="text/javascript">
				$('#proNameShow2').html("บันทึกข้อมูลอาจารย์สำเร็จ");
				$("#modalSuccess").modal("show");
			</script>
		<?php }
		else{
			?>
			<script type="text/javascript">
				$('#proNameShow2').html("บันทึกข้อมูลอาจารย์ไม่สำเร็จ เนื่องจากบัญชีผู้ใช้งานมีอยู่ในระบบแล้ว กรุณาลองใหม่อีกครั้ง");
				$("#modalSuccess").modal("show");
			</script>
			<?php
			
		} 
		
    }
}