<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectManagement extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
        $this->load->model('course_model');
    }
    public function deleteSubject()
    {
		$cIDOld = $this->input->post('cIDOld');
        $cID = $this->input->post('courseID');
		$cNameTH = $this->input->post('courseNameTH');
		$cNameEN = $this->input->post('courseNameEN');

        $data = array(
			"courseID" => $cID,
			"courseNameTH" => $cNameTH,
			"courseNameEN" => $cNameEN,
		);
		$courseOffered = $this->course_model->get_courseOffered($cID);
		if(!empty($courseOffered)){
			$this->course_model->delCourseOffered($data);
		}
        $this->course_model->delSub($data,$cIDOld);
		redirect(base_url('subjectManagement'));
    }

	// Can use insert //
	public function getRandomHex($num_bytes=4) {
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function insertSubject()
    {
        $courseID = $this->input->post('courseID-add-modal');
        $courseNameTH = $this->input->post('courseNameTH-add-modal');
        $courseNameEN = $this->input->post('courseNameEN-add-modal');
		$courseStatus = $this->input->post('coursestatus');
		$courseId = $this->getRandomHex(8);
		$data = array(
			"courseID" => $courseID,
			"courseNameTH" => $courseNameTH,
			"courseNameEN" => $courseNameEN,
			"courseId" => $courseId
		);
		// print_r($courseAll[0]['courseID']);
		// echo exit;?>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title prompt">เพิ่มรายวิชา</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body prompt">
					<p id="successShow"></p>
					</div>
					<div class="modal-footer prompt">
						<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
					</div>
				</div>
			</div>
		</div>
		<?php
		$courseCheck = $this->course_model->get_courseID($courseID);
		if(empty($courseCheck)){
			$insertId = $this->course_model->insertSubject($data);
			?>
			<script type="text/javascript">
			$('#successShow').html("บันทึกข้อมูลรายวิชาสำเร็จ");
			$("#modalSuccess").modal("show");
			</script>
		<?php 
		}
		else{ ?>
			<script type="text/javascript">
			$('#successShow').html("บันทึกข้อมูลรายวิชาไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
			$("#modalSuccess").modal("show");
			</script>
		<?php }
        // redirect(base_url('subjectManagement'));
    }

    public function updateSubject()
    {
        $cIDOld = $this->input->post('courseIDOld');
        $cID = $this->input->post('courseID');
		$cNameTH = $this->input->post('courseNameTH');
		$cNameEN = $this->input->post('courseNameEN');
		

		$data = array(
			"courseID" => $cID,
			"courseNameTH" => $cNameTH,
			"courseNameEN" => $cNameEN,
		);
		$this->course_model->updateSub($data,$cIDOld);   
        redirect(base_url('subjectManagement'));
	
	}
	public function index()
	{
		$data['course'] = $this->course_model->findAllCourse();
        $username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;

		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('subjectManagement/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');

	}
}