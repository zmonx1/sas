<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AttendanceList extends BD_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
    }
	public function index()
	{
		$username = $this->username;
		$userId = $this->session->userId;
		$user = $this->user_model->get_user($username);
		$userId = $user[0]['userId'];
		$data['term'] = $this->course_model->getTerm();
		$data['course'] = $this->course_model->findAllCourseOffer();
		$courseOffered = $this->course_model->findAllCourseOffered();
		$courseOfferedProfessor = [];
		$courseOfferedProfessorPrefixName = [];
		$courseOfferedProfessorName = [];
		$courseOfferedProfessorLastName = [];
		$findProfessorCourseOffered = [];
		$courseOfferedByProfessor = [];

		$numCourse = 0;
		
		
		for ($i=0; $i < sizeof($courseOffered) ; $i++) { 
			$findProfessorCourseOffered[$i] = $this->course_model->get_courseOfferedId($courseOffered[$i]['courseOfferedId']);
			$courseOfferedProfessorById[$i] = $findProfessorCourseOffered[$i][0]['teacherId'];
			$courseOfferedStudentById[$i] = $findProfessorCourseOffered[$i][0]['studentId'];

		}
		// print_r($courseOfferedProfessorById);
		// echo exit;
		$role = $this->session->role;
		if ($role  == '1') {
			for ($i=0; $i < sizeOf($findProfessorCourseOffered); $i++) { 
				$courseOfferedByProfessor[$i] = $findProfessorCourseOffered[$i][0]['courseOfferedId'];
			}
			// print_r($courseOfferedByProfessor);
			// echo exit;
		}
		elseif ($role == '2') {
			for ($i=0; $i < sizeof($courseOfferedStudentById); $i++) { 
				if(in_array($userId,($courseOfferedStudentById[$i]))){

					$courseOfferedByProfessor[$numCourse] = $findProfessorCourseOffered[$i][0]['courseOfferedId'];
					$numCourse++;
				}
			}
			// print_r(($courseOfferedByProfessor));
			// echo exit;
		}
		else{
			for ($i=0; $i < sizeof($courseOfferedProfessorById); $i++) { 
				if(in_array($userId,($courseOfferedProfessorById[$i]))){

					$courseOfferedByProfessor[$numCourse] = $findProfessorCourseOffered[$i][0]['courseOfferedId'];
					$numCourse++;
				}
			}
			// print_r(sizeOf($courseOfferedByProfessor));
			// echo exit;
		}
		// print_r($courseOfferedByProfessor);
		// echo exit;
		$numName = 0;
		$courseOfferedCount = [];
		$courseOfferedName = [];
		
		for ($i=0; $i < sizeof($courseOfferedByProfessor); $i++) { 
				$findCourseOfferedId[$i] = $this->course_model->get_courseOfferedId($courseOfferedByProfessor[$i]);
				$courseOfferedCount[$i] = $findCourseOfferedId[$i];
				// print_r($findCourseOfferedId[$i]);
				// echo exit;
				for ($j=0; $j < sizeof($findCourseOfferedId[$i][0]['teacherId']) ; $j++) { 
					$courseOfferedProfessor[$j] = $this->user_model->get_userDisplayName($findCourseOfferedId[$i][0]['teacherId'][$j]);
					
					$courseOfferedProfessorPrefixName[$i][$j] = $courseOfferedProfessor[$j][0]['prefix'];
					$courseOfferedProfessorName[$i][$j] = $courseOfferedProfessor[$j][0]['firstName'];
					$courseOfferedProfessorLastName[$i][$j] = $courseOfferedProfessor[$j][0]['lastName'];
					
				}
			
				$courseOfferedName[$i] = $this->course_model->findCourseOffered($findCourseOfferedId[$i][0]['courseId']);
				// $numName++;
				// print_r($courseOfferedName[$i]);
				// echo exit;
				
		}
		// print_r($courseOfferedProfessorName);
		// echo exit;
		$data['courseOfferedCount'] = $courseOfferedCount;
		$data['courseOfferedName'] = $courseOfferedName;
		$data['courseOffered'] = $courseOffered;
		$data['courseOfferedByProfessor'] = $courseOfferedByProfessor;
		$data['courseOfferedProfessor'] = $courseOfferedProfessor;
		$data['courseOfferedProfessorPrefixName'] = $courseOfferedProfessorPrefixName;
		$data['courseOfferedProfessorName'] = $courseOfferedProfessorName;
		$data['courseOfferedProfessorLastName'] = $courseOfferedProfessorLastName;
		$data['user'] = $user;
		
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('attendanceList/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');

	}
}