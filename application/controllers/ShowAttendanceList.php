<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowAttendanceList extends BD_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('checkclass_model');
		$this->load->model('attendance_model');


    }
	public function detailAttendance($courseOfferedId = NULL)
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$courseOfferedName = [];
		for ($i=0; $i < sizeOf($courseOffered); $i++) { 
			$courseOfferedName[$i] = $this->course_model->get_course($courseOffered[$i]['courseId']);
		}

		$courseOfferedProfessor = [];
		$courseOfferedProfessorPrefixName = [];
		$courseOfferedProfessorName = [];
		$courseOfferedProfessorLastName = [];
		$attendance = [];
		$attendanceCheckClassId = [];
		$studentAttendance =[];
		for ($i=0; $i < sizeof($courseOffered) ; $i++) { 

			for ($j=0; $j < sizeof($courseOffered[$i]['teacherId']) ; $j++) { 
				$courseOfferedProfessor[$j] = $this->user_model->get_userDisplayName($courseOffered[$i]['teacherId'][$j]);
				$courseOfferedProfessorPrefixName[$i][$j] = $courseOfferedProfessor[$j][0]['prefix'];
				$courseOfferedProfessorName[$i][$j] = $courseOfferedProfessor[$j][0]['firstName'];
				$courseOfferedProfessorLastName[$i][$j] = $courseOfferedProfessor[$j][0]['lastName'];
			}	
		}
		$checkClass = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedId);
		
		for ($i=0; $i < sizeOf($checkClass); $i++) { 
			$attendance[$i] = $this->attendance_model->getAttendanceByCheckClassId($checkClass[$i]['checkClassId']);
			
		}
	
		for ($i=0; $i < sizeOf($attendance); $i++) { 
			if(!empty($attendance[$i])){
				$attendanceCheckClassId[] = $attendance[$i][0]['checkClassId'];
				for ($j=0; $j < sizeOf($attendance[$i][0]['studentId']); $j++) { 
					$studentAttendance[$i][$j] = $attendance[$i][0]['studentId'][$j];
				}
			}
		}
		
		// $studentAttendance = array_unique($studentAttendance);
		// $studentAttendance = array_values($studentAttendance);
		// print_r($checkClass);
		// echo exit;
		
		$studentEnroll = [];
		$student = [];
		$status = [];
		for ($i=0; $i < sizeOf($courseOffered[0]['studentId']); $i++) { 
			$student[$i] = $this->user_model->get_StudentEnroll($courseOffered[0]['studentId'][$i]);
			
		}
		for ($i=0; $i < sizeOf($student); $i++) { 
			$studentEnroll [$i] = $student[$i][0]['studentId'];
		}
		for ($i=0; $i < sizeOf($checkClass); $i++) { 
			if(in_array($checkClass[$i]['checkClassId'],$attendanceCheckClassId)){
				if(!empty($studentAttendance[$i])){
					for ($j=0; $j < sizeOf($studentEnroll); $j++) { 
						if(in_array($studentEnroll[$j],$studentAttendance[$i])){
							$status[$i] = 1;
						}
						else{
							$status[$i] = 0;
						}
					}
				}else{
					$status[$i] = 0;
				}
			}else {
				$status[$i] = 0;
			}
		}
		// print_r($status);
		// echo exit;
		$data['status']  = $status;
		$data['attendanceCheckClassId'] = $attendanceCheckClassId;
		$data['attendance'] = $attendance;
		$data['checkClass'] = $checkClass;
		$data['studentAttendance'] = $studentAttendance;
		$data['student'] = $student;
		$data['user'] = $user;
		$data['courseOfferedName'] = $courseOfferedName;
		$data['courseOffered'] = $courseOffered;
		$data['courseOfferedProfessor'] = $courseOfferedProfessor;
		$data['courseOfferedProfessorPrefixName'] = $courseOfferedProfessorPrefixName;
		$data['courseOfferedProfessorName'] = $courseOfferedProfessorName;
		$data['courseOfferedProfessorLastName'] = $courseOfferedProfessorLastName;
		// print_r($courseOffered);
		// echo exit;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('showAttendanceList/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}


	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('showAttendanceList/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');

	}
}