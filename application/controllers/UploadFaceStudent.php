<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UploadFaceStudent extends BD_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('image_model');

	}
	public function uploadFileStudent()
	{

		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$studentID = $user[0]['studentId'];
		// print_r($_FILES['file']['name'][0]);
		// echo exit;
		$train_img_count = (70/ 100)* sizeOf($_FILES['file']['name']);
		$test_img_count = (30/ 100)* sizeOf($_FILES['file']['name']);
		$imageFile = [];
		if (!empty($_FILES['file']['name'])) {
			for ($i = 0; $i < sizeOf($_FILES['file']['name']); $i++) {
				if ($train_img_count > $i) {
					// Where the file is going to be stored
					$uploadDir = "C:/sas/facenet07/data/train_images";
					$filename =  $_FILES['file']['name'][$i];
					$filenameDir = $uploadDir . '/' . $studentID . '/' . $filename;
					$filenameDirExt = $uploadDir . '/' . $studentID;
					$tmpFile = $_FILES['file']['tmp_name'][$i];
					$imageFile[] = $filenameDir;
					// Check if file already exists
					if (!file_exists($filenameDirExt)) {
						mkdir($filenameDirExt);
						move_uploaded_file($tmpFile, $filenameDir);
					} else {
						move_uploaded_file($tmpFile, $filenameDir);
					}
				}else{
					// Where the file is going to be stored
					$uploadDir = "C:/sas/facenet07/data/test_images";
					$filename =  $_FILES['file']['name'][$i];
					$filenameDir = $uploadDir . '/' . $studentID . '/' . $filename;
					$filenameDirExt = $uploadDir . '/' . $studentID;
					$tmpFile = $_FILES['file']['tmp_name'][$i];
					$imageFile[] = $filenameDir;

					// Check if file already exists
					if (!file_exists($filenameDirExt)) {
						mkdir($filenameDirExt);
						move_uploaded_file($tmpFile, $filenameDir);
					} else {
						move_uploaded_file($tmpFile, $filenameDir);
					}
				}
			}
		}
		$data = array(
			"image" => $imageFile,
			"studentId" => $studentID,
		);
		$this->image_model->insertImage($data);
		redirect(base_url('UploadFaceStudent'));
		
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$status = 1;
		$imageStudent = $this->image_model->get_imageByStudentId($user[0]['studentId']);
		if(empty($imageStudent)){
			$status = 0;
		}

		$data['status'] = $status;
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header', $data);
		$this->load->view('uploadFaceStudent/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}
