<?php
defined('BASEPATH') or exit('No direct script access allowed');

class StudentManagement extends BD_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
	}
	public function index()
	{
		$student = $this->user_model->get_Student();
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$data['student'] = $student;

		$this->load->view('layout/head');
		$this->load->view('layout/header', $data);
		$this->load->view('studentManagement/content', $data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}

	public function updateStudent()
	{
		$StuIdCheck = $this->input->post('StuIdCheck');
		$stdIdStu = $this->input->post('StdId');
		$prefixStu = $this->input->post('prefixStu');
		$firstNameStu = $this->input->post('firstNameStu');
		$lastNameStu = $this->input->post('lastNameStu');
		$emailStu = $this->input->post('emailStu');


		$data = array(
			"studentId" => $stdIdStu,
			"prefix" => $prefixStu,
			"firstName" => $firstNameStu,
			"lastName" => $lastNameStu,
			"email" => $emailStu
		);

		$this->user_model->updateStudent($data, $StuIdCheck);
		redirect(base_url('studentManagement'));
	}

	public function deleteStudent()
	{

		$stuPrefix= $this->input->post('prefix');
		$stuFirstName = $this->input->post('firstName');
		$stuLastName = $this->input->post('lastName');
		$stuEmail = $this->input->post('email');
		$stuPass = $this->input->post('password');
		$stuRole = $this->input->post('role');
		$stuUserID = $this->input->post('userId');
		$stuStudentId = $this->input->post('studentId');
		$data = array(
			"prefix" => $stuPrefix,
			"firstName" => $stuFirstName,
			"lastName" => $stuLastName,
			"email" => $stuEmail,
			"password" => $stuPass,
			"role" => $stuRole,
			"userId" => $stuUserID,
			"studentId" => $stuStudentId
		);
		$user = $this->user_model->get_user($stuEmail);
		$stuUserId = $user[0]['userId'];
		$courseOffered = $this->course_model->findAllCourseOffered();
		$courseOfferedStudent = [];
		// print_r(sizeOf($courseOffered));
		// echo exit;
		for ($i = 0; $i < sizeOf($courseOffered); $i++) {
			print_r("1");
			for ($j = 0; $j < sizeOf($courseOffered[$i]['studentId']); $j++) {
				print_r("2");
				// echo exit;
				if ($stuUserId === $courseOffered[$i]['studentId'][$j]) {
				print_r("3");
				// echo exit;
					unset($courseOffered[$i]['studentId'][$j]);
					$courseOfferedStudent = array_values($courseOffered[$i]['studentId']);
					// print_r($courseOfferedStudent);
					// echo exit;
					
					$this->user_model->delStu($data);
					$this->user_model->delStuEnroll2($courseOfferedStudent, $courseOffered[$i]['courseOfferedId']);
				}
			}
		}
		$this->user_model->delStu($data);
		// print_r($courseOffered);
		// 	echo exit;
		
		redirect(base_url('StudentManagement'));
	}
	public function getRandomHex($num_bytes = 4)
	{
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function insertStudent()
	{
		$stuId = $this->input->post('stuId-add-modal');
		$stuPrefix = $this->input->post('stuPrefix-add-modal');
		$stuFirstName = $this->input->post('stuFirstName-add-modal');
		$stuLastName = $this->input->post('stuLastName-add-modal');
		$stuEmail = $this->input->post('stuEmail-add-modal');
		$stuPassword = "Swe_001";
		$stuRole = "2";
		$userId = $this->getRandomHex(8);

		$user = $this->user_model->get_user($stuEmail); ?>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (นักศึกษา)</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body prompt">
						<p id="stdShow"></p>
					</div>
					<div class="modal-footer prompt">
						<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
					</div>
				</div>
			</div>
		</div>
		<?php
		if (empty($user)) {
			$checkStdId = $this->user_model->check_stdId($stuId);
			if (empty($checkStdId)) {
				$data = array(
					"prefix" => $stuPrefix,
					"firstName" => $stuFirstName,
					"lastName" => $stuLastName,
					"email" => $stuEmail,
					"password" => $stuPassword,
					"studentId" => $stuId,
					"role" => $stuRole,
					"userId" => $userId
				);

				$this->user_model->insertAlluser($data);
		?>
				<script type="text/javascript">
					$('#stdShow').html("บันทึกข้อมูลนักศึกษาสำเร็จ");
					$("#modalSuccess").modal("show");
				</script>
			<?php } else {
			?>
				<script type="text/javascript">
					$('#stdShow').html("บันทึกข้อมูลนักศึกษาไม่สำเร็จ กรุณาตรวจสอบรหัสนักศึกษา");
					$("#modalSuccess").modal("show");
				</script>
			<?php	}
		} else {
			?>
			<script type="text/javascript">
				$('#stdShow').html("บันทึกข้อมูลนักศึกษาไม่สำเร็จ กรุณาตรวจสอบอีเมล");
				$("#modalSuccess").modal("show");
			</script>
<?php
		}
	}
}
