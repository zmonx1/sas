<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ScheduleToTeach extends BD_Controller {

	
	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
    }


	public function closeCourseOffered()
	{
		$courseIDOld = $this->input->post('courseIDOld');
        $courseStatus = $this->input->post('courseStatus');
        

        $data = array(
            "status" => $courseStatus
        );

        $this->course_model->updatecloseStatus($data,$courseIDOld);
        redirect(base_url('ScheduleToTeach'));
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['term'] = $this->course_model->getTerm();
		$data['course'] = $this->course_model->findAllCourseOffer();
		$courseOffered = $this->course_model->findAllCourseOfferedStatus();
		// print_r($courseOffered);
		// echo exit;
		$courseOfferedName = [];
		for ($i=0; $i < sizeOf($courseOffered); $i++) { 
			$courseOfferedName[$i] = $this->course_model->get_course($courseOffered[$i]['courseId']);
		}
		// print_r($courseOfferedName[2][0]['courseID']);
		// echo exit;
		$courseOfferedProfessor = [];
		$courseOfferedProfessorPrefixName = [];
		$courseOfferedProfessorName = [];
		$courseOfferedProfessorLastName = [];
		
		// for ($i=0; $i < sizeof($courseOfferedName) ; $i++) { 
		// 	$courseOffered[$i] = $this->course_model->get_courseOffered($courseOfferedName[$i]['courseID']);
		// }
		// print_r($courseOffered[0]['teacherId']);
		// echo exit;
		for ($i=0; $i < sizeof($courseOffered) ; $i++) { 
			 for ($j=0; $j < sizeof($courseOffered[$i]['teacherId']) ; $j++) {
				$courseOfferedProfessor[$j] = $this->user_model->get_userDisplayName($courseOffered[$i]['teacherId'][$j]);
				$courseOfferedProfessorPrefixName[$i][$j] = $courseOfferedProfessor[$j][0]['prefix'];
				$courseOfferedProfessorName[$i][$j] = $courseOfferedProfessor[$j][0]['firstName'];
				$courseOfferedProfessorLastName[$i][$j] = $courseOfferedProfessor[$j][0]['lastName'];
	
			 }	
		}
		// print_r($courseOfferedProfessorName[0][0]);
		// echo exit;
		$data['courseOfferedName'] = $courseOfferedName;
		$data['courseOffered'] = $courseOffered;
		$data['courseOfferedProfessor'] = $courseOfferedProfessor;
		$data['courseOfferedProfessorPrefixName'] = $courseOfferedProfessorPrefixName;
		$data['courseOfferedProfessorName'] = $courseOfferedProfessorName;
		$data['courseOfferedProfessorLastName'] = $courseOfferedProfessorLastName;

		// print_r($courseOffered);
		// echo exit;
		$data['user'] = $user;

		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('scheduleToTeach/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
	public function getRandomHex($num_bytes = 4)
	{
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function courseOffered()
    {
        $courseID = $this->input->post('courseID-add-modal');
		$course_arr = explode (",", $courseID);
        $term = $this->input->post('term-add-modal');
        $year = $this->input->post('year-add-modal');
		$startDate = $this->input->post('startDate-add');
		$endDate = $this->input->post('endDate-add');

		$yearS = date('Y',strtotime($startDate));
		$day = date('d',strtotime($startDate));
		$month = date('m',strtotime($startDate));
		$yearS = $yearS-543;
		$yearS = strval($yearS);
		$Stdate = $day.'-'.$month.'-'.$yearS;
		$startDate = date("d-m-Y", strtotime($Stdate));

		$yearE = date('Y',strtotime($endDate));
		$dayE = date('d',strtotime($endDate));
		$monthE = date('m',strtotime($endDate));
		$yearE = $yearE-543;
		$yearE = strval($yearE);
		$Edate = $dayE.'-'.$monthE.'-'.$yearE;
		$endDate = date("d-m-Y", strtotime($Edate));

		// print_r($dateS);
		// echo exit;

		$courseId = [];
		$courseOffered = [];
		$courseTerm = [];
		$courseYear = [];
		$courseName = [];
		$n = 0;
		?>
			<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
			<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
			<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title prompt">เปิดสอนรายวิชา</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body prompt">
						<p id="messageShow"></p>
						</div>
						<div class="modal-footer prompt">
							<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
						</div>
					</div>
				</div>
			</div>
		<?php
		for ($i=0; $i < sizeOf($course_arr); $i++) { 

			$courseId[$i] = $this->course_model->get_courseByCourseID($course_arr[$i]);

			// print_r($courseId[$i]);
			// echo exit;
			$courseOffered = $this->course_model->get_courseOffered($courseId[$i][0]['courseId']);
			
			for ($j=0; $j < sizeOf($courseOffered); $j++) { 
				$courseTerm[$i][$n] = $courseOffered[$j]['term'];
				$courseYear[$i][$n] = $courseOffered[$j]['year'];
				$courseName[$i][$n] = $courseOffered[$j]['courseId'];
				$n++;
			}
		
			if(empty($courseOffered)){
				$data = array( 
					"courseId" => $courseId[$i][0]['courseId'],
					"term" => $term,
					"year" => $year,
					"studentId" => array(),
					"teacherId" => array(),
					"startDate" => $startDate,
					"endDate" => $endDate,
					"courseOfferedId" => $this->getRandomHex(8),
					"status" => "1"
				);
				$this->course_model->insertCourseOffered($data);
				// redirect(base_url('ScheduleToTeach'));
			}
			else{
				// print_r($courseId[$i]);
				// echo exit;
				if(in_array($courseId[$i][0]['courseId'],$courseName[$i])){
			
					if(in_array($term,$courseTerm[$i])){
					
						if(in_array((string)$year,$courseYear[$i])){
							?>
							<script type="text/javascript">
								$('#messageShow').html("เปิดสอนรายวิชาไม่สำเร็จ กรุณาตรวจสอบรายวิชาที่เปิดสอนใหม่อีกครั้ง");
								$("#modalSuccess").modal("show");
							</script>
							<?php
						}else{
							$data = array( 
								"courseId" => $courseId[$i][0]['courseId'],
								"term" => $term,
								"year" => $year,
								"studentId" => array(),
								"teacherId" => array(),
								"startDate" => $startDate,
								"endDate" => $endDate,
								"courseOfferedId" => $this->getRandomHex(8),
								"status" => "1"
							);
							$this->course_model->insertCourseOffered($data);
							// redirect(base_url('ScheduleToTeach'));
						}
					}else{
						$data = array( 
							"courseId" => $courseId[$i][0]['courseId'],
							"term" => $term,
							"year" => $year,
							"studentId" => array(),
							"teacherId" => array(),
							"startDate" => $startDate,
							"endDate" => $endDate,
							"courseOfferedId" => $this->getRandomHex(8),
							"status" => "1"
						);
						$this->course_model->insertCourseOffered($data);
						// redirect(base_url('ScheduleToTeach'));
					}
				}else{
					$data = array( 
						"courseId" => $courseId[$i][0]['courseId'],
						"term" => $term,
						"year" => $year,
						"studentId" => array(),
						"teacherId" => array(),
						"startDate" => $startDate,
						"endDate" => $endDate,
						"courseOfferedId" => $this->getRandomHex(8),
						"status" => "1"
					);
					$this->course_model->insertCourseOffered($data);
					// redirect(base_url('ScheduleToTeach'));
				}
			}
			
		}
		redirect(base_url('ScheduleToTeach'));

		
    }
}