<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckAttendance extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('checkclass_model');
		$this->load->model('attendance_model');


    }
	public function setPredict($checkClassId = NULL)
	{
		// --------- predict -------------
		// print_r($checkClassId);
		// echo exit;
		$url = "http://127.0.0.1:8086/predict/".$checkClassId;
		// Initialize a CURL session.
		$ch = curl_init();
		// Return Page contents.
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//grab URL and pass it to the variable.
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		$predict = array_unique(json_decode($result));

		$arrayPredict = array_values($predict);
		$data = array(
			"checkClassId" => $checkClassId,
			"studentId" => $arrayPredict
		);
		$this->attendance_model->insertAttendance($data);

		$checkClass = $this->checkclass_model->getClassByCheckClassId($checkClassId);
		$courseOffered = $this->course_model->get_courseOfferedId($checkClass[0]['courseOfferedId']);
		$attendance = $this->attendance_model->getAttendanceByCheckClassId($checkClassId);
		// print_r($attendance[0]['studentId']);
		// echo exit;
		$studentEnroll = [];
		$student = [];
		for ($i=0; $i < sizeOf($courseOffered[0]['studentId']); $i++) { 
			$student[$i] = $this->user_model->get_StudentEnroll($courseOffered[0]['studentId'][$i]);
			
		}
		for ($i=0; $i < sizeOf($student); $i++) { 
			$studentEnroll [$i] = $student[$i][0]['studentId'];
		}
		for ($i=0; $i < sizeOf($attendance[0]['studentId']); $i++) { 
			if(!(in_array($attendance[0]['studentId'][$i],$studentEnroll))){
				unset($attendance[0]['studentId'][$i]);
			}
		}
		$attendanceStudentId =  array_values($attendance[0]['studentId']);
		$this->attendance_model->delStuAttendance($attendanceStudentId,$checkClassId);
		redirect(base_url('checkAttendance/detailCheckAttendance/'.$checkClassId.''));

	}
	public function detailCheckAttendance($checkClassId = NULL){
		$checkClass = $this->checkclass_model->getClassByCheckClassId($checkClassId);
		$courseOfferedId = $checkClass[0]['courseOfferedId'];
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$course = $this->course_model->get_course($courseOffered[0]['courseId']);
		$courseOfferedProfessor = [];
		$courseOfferedProfessorPrefixName = [];
		$courseOfferedProfessorName = [];
		$courseOfferedProfessorLastName = [];
		for ($i=0; $i < sizeof($courseOffered) ; $i++) { 
			for ($j=0; $j < sizeof($courseOffered[$i]['teacherId']) ; $j++) {
				$courseOfferedProfessor[$j] = $this->user_model->get_userDisplayName($courseOffered[$i]['teacherId'][$j]);
				$courseOfferedProfessorPrefixName[$i][$j] = $courseOfferedProfessor[$j][0]['prefix'];
				$courseOfferedProfessorName[$i][$j] = $courseOfferedProfessor[$j][0]['firstName'];
				$courseOfferedProfessorLastName[$i][$j] = $courseOfferedProfessor[$j][0]['lastName'];
			}	
		}
		$attendance = $this->attendance_model->getAttendanceByCheckClassId($checkClassId);
		$studentEnroll = [];
		$student = [];
		for ($i=0; $i < sizeOf($courseOffered[0]['studentId']); $i++) { 
			$student[$i] = $this->user_model->get_StudentEnroll($courseOffered[0]['studentId'][$i]);
			
		}
		for ($i=0; $i < sizeOf($student); $i++) { 
			$studentEnroll [$i] = $student[$i][0]['studentId'];
		}
		$data['student'] = $student;
		$data['attendance'] = $attendance;
		$data['checkClass'] = $checkClass;
		$data['user'] = $user;
		$data['course'] = $course;
		$data['courseOffered'] = $courseOffered;
		$data['courseOfferedProfessor'] = $courseOfferedProfessor;
		$data['courseOfferedProfessorPrefixName'] = $courseOfferedProfessorPrefixName;
		$data['courseOfferedProfessorName'] = $courseOfferedProfessorName;
		$data['courseOfferedProfessorLastName'] = $courseOfferedProfessorLastName;
		
		// print_r($courseOfferedProfessor);
		// echo exit;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('checkAttendance/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
	public function changeStatus()
	{
		$status= $this->input->post('status');
		// print_r($status);
		// echo exit;
		$studentAttten =[];
		$studentId = $this->input->post('stdId');
		$checkClassId= $this->input->post('checkClass');
		$attendance = $this->attendance_model->getAttendanceByCheckClassId($checkClassId);
		$attendance[0]['studentId'][] = $studentId;
		$attendanceStudentId =  array_values($attendance[0]['studentId']);
		if($status == "เข้าเรียน"){
			
			$data = array(
				"checkClassId" => $checkClassId,
				"studentId" => $attendanceStudentId
			);
			$this->attendance_model->insertStudent($data);
		}else{
			for ($i=0; $i < sizeOf($attendance[0]['studentId']); $i++) { 
				if($studentId != $attendance[0]['studentId'][$i]){
					$studentAttten[] = $attendance[0]['studentId'][$i];
				}
			}
			$dataOld = array(
				"checkClassId" => $attendance[0]['checkClassId'],
				"studentId" => $attendance[0]['studentId']
			);
			$this->attendance_model->delStd($dataOld);
			
			$studentAttten = array_values($studentAttten);
			$data = array(
				"checkClassId" => $checkClassId,
				"studentId" => $studentAttten
			);
			$this->attendance_model->insertAttendance($data);
		}
		redirect(base_url('checkAttendance/detailCheckAttendance/'.$checkClassId.''));
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('checkAttendance/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}