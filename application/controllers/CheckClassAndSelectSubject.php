<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckClassAndSelectSubject extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('schedule_model');
		$this->load->model('checkclass_model');
    }
	public function uploadFileCapture($checkClassId = NULL)
	{
		$checkClass  =  $this->checkclass_model->getClassByCheckClassId($checkClassId);
		$courseOfferedId = $checkClass[0]['courseOfferedId'];
		// print_r(json_encode($checkClassId)) ;
		// echo exit;
		$filenameDirExt ='';
		if (!empty($_FILES['file']['name'])) {
			for ($i = 0; $i < sizeOf($_FILES['file']['name']); $i++) {
				// Where the file is going to be stored
				$uploadDir = "C:/sas/facenet07/data/images";
				$filename =  $_FILES['file']['name'][$i];
				$filenameDir = $uploadDir . '/' . $checkClassId .'/' . $filename;
				$filenameDirExt = $uploadDir . '/' .$checkClassId;
				$tmpFile = $_FILES['file']['tmp_name'][$i];

				// Check if file already exists
				if (!file_exists($filenameDirExt)) {
					mkdir($filenameDirExt);
					move_uploaded_file($tmpFile, $filenameDir);
				} else {
					move_uploaded_file($tmpFile, $filenameDir);
				}
			}

			$data = array(
				"date" => $checkClass[0]['date'],
				"startTime" => $checkClass[0]['startTime'],
				"endTime" => $checkClass[0]['endTime'],
				"courseOfferedId" => $checkClass[0]['courseOfferedId'],
				"status" => "1",
				"checkClassId" => $checkClass[0]['checkClassId'],
				"day" => $checkClass[0]['day']
			);
			$this->checkclass_model->updateStatus($data);
		}
		// echo  $checkClassId;
		// redirect(base_url('checkAttendance/detailCheckAttendance/'.$courseOfferedId));
	}
	public function detailCheckClass($courseOfferedId = NULL)
	{
		
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$course = $this->course_model->get_course($courseOffered[0]['courseId']);
		$checkClass = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedId);
		
		$data['checkClass'] = $checkClass;
		$data['user'] = $user;
		$data['course'] = $course;
		$data['courseOffered'] = $courseOffered;

		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('checkClassAndSelectSubject/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
	public function getRandomHex($num_bytes = 4)
	{
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function insertCheckClass($courseOfferedId = NULL)
	{
		$date= $this->input->post('date');

		// print_r($date);
		// echo exit;

		// $date = date("d-m-Y", strtotime($date));
	
		$year = date('Y',strtotime($date));
		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = $year-543;
		$year = strval($year);
		$Stdate = $month.'-'.$day.'-'.$year;
		// $newDate = date("d-m-Y", strtotime($Stdate));
		// print_r($Stdate);
		// echo exit;
	
		// $date = $date->format('d-m-Y');
		$startTime= $this->input->post('startTime');
		$endTime= $this->input->post('endTime');
		$dateName = date('l', strtotime($Stdate));
		$data = array(
			"date" => $Stdate,
			"startTime" => $startTime,
			"endTime" => $endTime,
			"checkClassId" => $this->getRandomHex(8),
			"courseOfferedId" => $courseOfferedId,
			"status" => "0",
			"day" => $dateName
		);
		$this->checkclass_model->insertCheckClass($data);
		redirect(base_url('CheckClassAndSelectSubject/detailCheckClass/'.$courseOfferedId.''));
	}

	public function deleteCheckClass()
    {	
		$date= $this->input->post('date');
		$startTime = $this->input->post('startTime');
		$endTime = $this->input->post('endTime');
		$checkClassId = $this->input->post('checkClassId');
		$courseOfferedId = $this->input->post('courseOfferedId');
		$status = $this->input->post('status');
		$day = $this->input->post('day');
		$data = array(
			"date" => $date,
			"startTime" => $startTime,
			"endTime" => $endTime,
			"checkClassId" => $checkClassId,
			"courseOfferedId" => $courseOfferedId,
			"status" => $status,
			"day" => $day,
		);
        $this->checkclass_model->delCheckClass($data);
		redirect(base_url('checkClassAndSelectSubject/detailCheckClass/'.$courseOfferedId.''));
    }

	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('checkClassAndSelectSubject/content');
		$this->load->view('layout/foot');            
		$this->load->view('layout/footer');
	}
}