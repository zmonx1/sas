<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RePassword extends BD_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->helper('url');
	}
	public function updateNewPassword()
	{
		$username = $this->username;
		$oldpassword = $this->input->post('password');
		$newpassword = $this->input->post('newPassword');
		$confirmpassword = $this->input->post('confirmPassword');
		$val = $this->user_model->get_user($username);
		
		$match = $val[0]['password'];
		if (!empty($val)) {
			if($newpassword != $confirmpassword){
				?>
				<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
				<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
				<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
				<div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-3">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title prompt">รหัสผ่านใหม่ไม่ตรงกัน</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body prompt">
								<p>โปรดกรอกรหัสผ่านให้ตรงกัน</p>
							</div>
							<div class="modal-footer prompt">
								<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
							</div>
						</div>
					</div>
				</div>
				<script>
					$("#fire-modal-3").modal("show");
				</script>
			<?php } 
			
			else{ 
				$re = $this->user_model->Repassword($val, $newpassword, $oldpassword);
				if($re == "true") {
				$re;
			?>
				<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
				<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
				<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
				<div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-4">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title prompt">รหัสผ่านถูกต้อง</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body prompt">
								<p>แก้ไขรหัสผ่านสำเร็จ</p>
							</div>
							<div class="modal-footer prompt">
								<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
						$("#fire-modal-4").modal("show");
				</script>
			<?php
			} elseif ($re == "false") {
				$re;

			?>
				<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
				<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
				<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
				<div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-3">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title prompt">รหัสผ่านไม่ถูกต้อง</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body prompt">
								<p>โปรดกรอกรหัสผ่านเดิมให้ถูกต้อง</p>
							</div>
							<div class="modal-footer prompt">
								<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
							</div>
						</div>
					</div>
				</div>
				<script>
					$("#fire-modal-3").modal("show");
				</script>
				<?php
			}
		}}
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('rePassword/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
		
	}
}
