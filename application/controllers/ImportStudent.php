<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ImportStudent extends BD_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		// $this->load->library('excel');
	}
	public function getRandomHex($num_bytes = 4)
	{
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function uploadStudent()
	{
		if (isset($_POST['upload'])) {
			$fileUpload = $_FILES['file']['tmp_name'];  // ไฟล์ที่อัพโหลด

			// ตรวจสอบว่ามีการอัพโหลดไฟล์หรือไม่
			if (isset($fileUpload) && $fileUpload != "") {

				// โหลด excel library
				$this->load->library('excel');

				// อ่านไฟล์จาก path temp ชั่วคราวที่เราอัพโหลด
				$objPHPExcel = PHPExcel_IOFactory::load($fileUpload);

				// ดึงข้อมูลของแต่ละเซลในตารางมาไว้ใช้งานในรูปแบบตัวแปร array
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				// print_r($cell_collection);
				// echo exit;
				// วนลูปแสดงข้อมูล
				//extract to a PHP readable array format
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

					//The header will/should be in row 1 only. of course, this can be modified to suit your need.
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
					}
				}
				// print_r(count($arr_data));
				// echo exit;
				for ($i = 2; $i < sizeOf($arr_data)+2; $i++) {
					if ((!empty($arr_data[$i]['A'])) && (!empty($arr_data[$i]['B'])) && (!empty($arr_data[$i]['C'])) && (!empty($arr_data[$i]['D'])) && (!empty($arr_data[$i]['E']))) {
						// set data 
						$stuId = strval($arr_data[$i]['A']);
						$stuPrefix = $arr_data[$i]['B'];
						$stuFirstName = $arr_data[$i]['C'];
						$stuLastName = $arr_data[$i]['D'];
						$stuEmail = $arr_data[$i]['E'];
						$stuPassword = "Swe_001";
						$stuRole = "2";
						$userId = $this->getRandomHex(8);

						$user = $this->user_model->get_user($stuEmail);

						$user = $this->user_model->get_user($stuEmail); ?>
						<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
						<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
						<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
						<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
							<div class="modal-dialog modal-md" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (นักศึกษา)</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body prompt">
										<p id="stdShow"></p>
									</div>
									<div class="modal-footer prompt">
										<a href="<?php echo base_url("StudentManagement") ?>" class="btn btn-primary">ตกลง</a>
									</div>
								</div>
							</div>
						</div>
						<?php
						if (empty($user)) {
							$checkStdId = $this->user_model->check_stdId($stuId);
							if (empty($checkStdId)) {
								$data = array(
									"prefix" => $stuPrefix,
									"firstName" => $stuFirstName,
									"lastName" => $stuLastName,
									"email" => $stuEmail,
									"password" => $stuPassword,
									"studentId" => $stuId,
									"role" => $stuRole,
									"userId" => $userId
								);
								$this->user_model->insertAlluser($data);
						?>
								<script type="text/javascript">
									$('#stdShow').html("บันทึกข้อมูลนักศึกษาสำเร็จ");
									$("#modalSuccess").modal("show");
								</script>
							<?php
							} else {
							?>
								<script type="text/javascript">
									$('#stdShow').html("บันทึกข้อมูลนักศึกษาไม่สำเร็จ กรุณาตรวจสอบรหัสนักศึกษา");
									$("#modalSuccess").modal("show");
								</script>
							<?php
							}
						} else {
							?>
							<script type="text/javascript">
								$('#stdShow').html("บันทึกข้อมูลนักศึกษาไม่สำเร็จ กรุณาตรวจสอบอีเมล");
								$("#modalSuccess").modal("show");
							</script>
<?php
						}
					}
				}


				//send the data in an array format
				// $data['header'] = $header;
				// $data['values'] = $arr_data;
				// print_r($header);
				// return $data;
			}
		}
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;

		$this->load->view('layout/head');
		$this->load->view('layout/header', $data);
		$this->load->view('importStudent/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}
