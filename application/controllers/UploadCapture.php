<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UploadCapture extends BD_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$userId = $user[0]['userId'];
		// print_r($user[0]['userId']);
		// echo exit;
		$courseOfferedNameAll = $this->course_model->findAllCourseOffered();
		$userId = $this->session->userId;
	
		$courseOfferedName = [];
		$numCourse = 0;
		$courseOfferedByProfessor = [];
	
		$courseOfferedCount = [];
		// print_r($courseOfferedNameAll);
		// echo exit;
		for ($i = 0; $i < sizeof($courseOfferedNameAll); $i++) {
			// print_r($courseOfferedNameAll[$i]['courseOfferedId']);
			// echo exit;
			$findProfessorCourseOffered[$i] = $this->course_model->get_courseOfferedId($courseOfferedNameAll[$i]['courseOfferedId']);
			// print_r($findProfessorCourseOffered[$i][0]['teacherId']);
			// echo exit;
			$courseOfferedProfessorById[$i] = $findProfessorCourseOffered[$i][0]['teacherId'];
		}
		;
		for ($i = 0; $i < sizeof($courseOfferedProfessorById); $i++) {
			// print_r($courseOfferedProfessorById[$i]);
			// echo exit;
			if (in_array($userId, ($courseOfferedProfessorById[$i]))) {

				$courseOfferedByProfessor[$numCourse] = $findProfessorCourseOffered[$i][0]['courseOfferedId'];
				$numCourse++;
			}
		}
		for ($i=0; $i < sizeof($courseOfferedByProfessor); $i++) { 
			$findCourseOfferedId[$i] = $this->course_model->get_courseOfferedId($courseOfferedByProfessor[$i]);
			$courseOfferedCount[$i] = $findCourseOfferedId[$i];
			$courseOfferedName[$i] = $this->course_model->findCourseOffered($findCourseOfferedId[$i][0]['courseId']);
			
		}
		// for ($i = 0; $i < sizeof($courseOfferedByProfessor); $i++) {
		// 	$courseOfferedName[$i] = $this->course_model->findCourseOffered($courseOfferedByProfessor[$i][0]['courseId']);
		// }
		// print_r($courseOfferedName);
		// echo exit;
		$data['courseOfferedCount'] = $courseOfferedCount;
		$data['courseOfferedName'] = $courseOfferedName;
		$data['courseOffered'] = $courseOfferedByProfessor;
		// print_r($courseOfferedByProfessor);
		// echo exit;
		$data['user'] = $user;

		$this->load->view('layout/head');
		$this->load->view('layout/header', $data);
		$this->load->view('uploadCapture/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}
