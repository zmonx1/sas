<?php
header("Access-Control-Allow-Origin:*;");
defined('BASEPATH') OR exit('No direct script access allowed');

class TrainFaceStudent extends BD_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('image_model');

    }
	public function trainStudent()
	{
		header("Access-Control-Allow-Origin:*;");
		$studentImage = $this->image_model->findAllStudentImage();
		$studentAll = $this->user_model->get_Student();
		
		$studentIdNotUpload = [];
		$countStudentUp = 0;
		$countStudentNotUp = 0;

		for ($i=0; $i < sizeOf($studentAll); $i++) { 
			for ($j=0; $j < sizeOf($studentImage); $j++) { 
				if($studentImage[$j]['studentId'] == $studentAll[$i]['studentId']){
					$countStudentUp++;
				}else{
					$studentIdNotUpload[] = $studentAll[$i]['studentId'];
					$countStudentNotUp++;
				}
			}
		}
		// print_r($countStudentNotUp);
		// echo exit;
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$data['studentIdNotUpload'] = $studentIdNotUpload;
		$data['countStudentUp'] = $countStudentUp;
		$data['countStudentNotUp'] = $countStudentNotUp;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('trainFaceStudent/trainpage',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
	public function index()
	{
		$studentImage = $this->image_model->findAllStudentImage();
		$studentAll = $this->user_model->get_Student();
		
		$studentIdAll = [];
		$countStudentUp = 0;
		$countStudentNotUp = 0;
		$studentIdImage = [];
		// print_r($studentImage);
		// echo exit;

		for ($i=0; $i < sizeOf($studentAll); $i++) { 
			for ($j=0; $j < sizeOf($studentImage); $j++) { 
				if($studentImage[$j]['studentId'] == $studentAll[$i]['studentId']){
					$countStudentUp++;
					$studentIdImage[$i]= $studentImage[$j]['studentId'];
				}
				$studentIdAll[$i] = $studentAll[$i]['studentId'];
			}
		}
		$studentIdImage = array_values($studentIdImage);
		$studentIdAll = array_values($studentIdAll);
		
		$studentIdNotUpload = array_diff($studentIdAll,$studentIdImage);

		// $studentIdNotUpload = array_unique($studentIdNotUpload);
		$countStudentNotUp = count($studentIdNotUpload);

		// print_r($studentIdNotUpload);
		// echo exit;
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$data['studentIdNotUpload'] = $studentIdNotUpload;
		$data['countStudentUp'] = $countStudentUp;
		$data['countStudentNotUp'] = $countStudentNotUp;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('trainFaceStudent/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}