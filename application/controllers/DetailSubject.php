<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailSubject extends BD_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('schedule_model');
		$this->load->model('checkclass_model');

    }
	public function getRandomHex($num_bytes = 4)
	{
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function showDetail($courseOfferedId = NULL)
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);

		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);

		// print_r($courseOffered);
		// echo exit;
		// ---------- role of userType ---------------- //
		$professor = $this->user_model->get_Professor();
		$academician = $this->user_model->get_Academician();

		$result = array_merge($professor, $academician);
		// print_r($result);
		// echo exit;
		// ------------- StudentEnroll List  ----------- //
		$student = $this->user_model->get_Student();
		$studentEnroll = [];
		// print_r($courseOffered);
		// echo exit;
		for ($i=0; $i < sizeof($courseOffered[0]['studentId']) ; $i++) {
			// print_r($courseOffered[0]['studentId'][$i]);
			// echo exit;
			$studentEnroll[$i] = $this->user_model->get_StudentEnroll($courseOffered[0]['studentId'][$i]);
		}
		$data['studentEnroll'] = $studentEnroll;

		// print_r($student[1]['studentId']);
		// echo exit;
		// ------------- Schedule  ----------- //
		$scheduleTeach = [];
		// print_r($courseOffered[0]['studentId'][1]);
		// echo exit;
		$j = 0;
		for ($i=0; $i < sizeof($courseOffered) ; $i++) {
			$scheduleTeach = $this->course_model->get_ScheduleToTeach($courseOffered[0]['courseOfferedId']);
		}
		$data['scheduleTeach'] = $scheduleTeach;
			// print_r($scheduleTeach);
			// echo exit;
		$studentId = [];
		$studentIdStr = '';
		$studentNum = 0;
		for ($i=0; $i < sizeOf($student) ; $i++) { 
			$studentIdStr = substr($student[$i]['studentId'],0,2);
			if (!in_array($studentIdStr, $studentId)) {
				$studentId[$studentNum] = $studentIdStr;
				$studentNum++;
			} ;
		}
		$data['studentId'] = $studentId;
		// print_r($studentId);
		// echo exit;

		//------------- deleteStudentEnroll ---------------//
		$delStudentEnroll = $this->input->post('studentUserId');
			// print_r($delStudentEnroll);
			// echo exit;
		$indexStudent = 0;
		if (isset($delStudentEnroll)) {
			// print_r($delStudentEnroll);
			// echo exit;
			for ($i=0; $i < sizeof($courseOffered[0]['studentId']); $i++) { 
				if($courseOffered[0]['studentId'][$i] == $delStudentEnroll){
					$indexStudent = $i;
				}
			}
			unset($courseOffered[0]['studentId'][$indexStudent]);
			$courseOfferedStudent = $courseOffered[0]['studentId'];
			$courseOfferedStudent = array_values($courseOfferedStudent);
			// print_r($courseOfferedStudent);
			// echo exit;
			$this->user_model->delStuEnroll($courseOfferedStudent,$courseOffered[0]['courseOfferedId']);
			// print_r($delStudentEnroll);
			// echo exit;
		}
		//////////////// --------- period --------- /////////////////////////////

		$checkClass = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedId);
		
		
		// print_r($checkClass);
		// echo "<pre>";
		// print_r($countPeriod);
		// echo exit;

		$data['checkClass'] = $checkClass;
		// $data['countPeriod'] = $countPeriod;
		$data['course'] = $this->course_model->get_course($courseOffered[0]['courseId']);
		$data['courseOfferedProfessor'] = $courseOffered[0]['teacherId'];
		$data['professor'] = $result;
		$data['student'] = $student;
		$data['user'] = $user;
		$data['courseOffered'] = $courseOffered;

		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('detailSubject/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
		
	}
	public function insertSchedule(){
		$courseOfferedId = $this->input->post('courseIDOld');
		$dayTeach = $this->input->post('dayTeach-add');
		$startTime = $this->input->post('startTime-add');
		$endTime = $this->input->post('endTime-add');

		$data = array(
			"courseOfferedId" => $courseOfferedId,
			"day" => $dayTeach,
			"startDate" => $startTime,
			"endDate" => $endTime
		);

		$this->course_model->insertScheduleToTeach($data);
		$courseSetCheck = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedId);
		// print_r($courseSetCheck);
		// echo exit;
		for ($i=0; $i < sizeOf($courseSetCheck); $i++) { 
			$data = array(
				"date" => $courseSetCheck[$i]['date'],
				"startTime" => $courseSetCheck[$i]['startTime'],
				"endTime" => $courseSetCheck[$i]['endTime'],
				"courseOfferedId" => $courseSetCheck[$i]['courseOfferedId'],
				"status" => "0",
				"day" => $courseSetCheck[$i]['day']
			);
			$this->checkclass_model->delCourseOffered($data,$courseOfferedId);
		}

		$allDate = array(); 
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$period = new DatePeriod(
			new DateTime($courseOffered[0]['startDate']),
			new DateInterval('P1D'),
			new DateTime($courseOffered[0]['endDate'])
		);
		foreach ($period as $key => $value) {
			$Store = $value->format('d-m-Y');
			$allDate[] = $Store;
		}
		$allDate[] = $courseOffered[0]['endDate'];
		$getDayByCourseOfferedId = $this->schedule_model->getDayByCourseOfferedId($courseOffered[0]['courseOfferedId']);
		// print_r($getDayByCourseOfferedId);
		// echo exit;
		$coursePeriod = [];
		$countPeriod = 0;
		
		for ($i=0; $i < sizeof($allDate) ; $i++) { 
    		$dateName = date('l', strtotime($allDate[$i]));
			$dayName = array("date"=>$allDate[$i]); 
			for ($j=0; $j < sizeof($getDayByCourseOfferedId) ; $j++) { 
				if ($dateName == $getDayByCourseOfferedId[$j]['day']) {
					$getDayByCourseOfferedId[$j] = array_merge($getDayByCourseOfferedId[$j],$dayName);
					$coursePeriod[$countPeriod] = $getDayByCourseOfferedId[$j];
					$countPeriod++;

				}
			}
		}
		// print_r($coursePeriod);
		// echo exit;
		for ($i=0; $i < sizeOf($coursePeriod); $i++) { 
			$data = array(
				"date" => $coursePeriod[$i]['date'],
				"startTime" => $coursePeriod[$i]['startDate'],
				"endTime" => $coursePeriod[$i]['endDate'],
				"checkClassId" => $this->getRandomHex(8),
				"courseOfferedId" => $coursePeriod[$i]['courseOfferedId'],
				"status" => "0",
				"day" => $coursePeriod[$i]['day']
			);
			$this->checkclass_model->insertCheckClass($data);
		}
		
		redirect(base_url('detailSubject/showDetail/'.$courseOfferedId.''));

	}

	public function insertProfessor()
    {
		$courseOfferedId = $this->input->post('courseIDOld');
		$insertPro = $this->input->post('insertProadd');
		// print_r($insertPro);
		// echo exit;
		$professor_arr = explode (",", $insertPro); 
   
        $data = array(
            "teacherId" => (array)$professor_arr,
        );

        $this->course_model->insertProandStu_detailSubject($data,$courseOfferedId);
        redirect(base_url('detailSubject/showDetail/'.$courseOfferedId.''));
    }

	// รายคน
	public function insertStudent()
    {
		$courseOfferedId = $this->input->post('courseIDOld');
		$insertStu = $this->input->post('insertStuadd');
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$student_arr = explode (",", $insertStu); 
		$result = array_merge($courseOffered[0]['studentId'], $student_arr);
		$result = array_unique($result);
		$result = array_values($result);
        $data = array(
            "studentId" => (array)$result,
        );
		// print_r($result);
		// echo exit;
        $this->course_model->insertProandStu_detailSubject($data,$courseOfferedId);
        redirect(base_url('detailSubject/showDetail/'.$courseOfferedId.''));
    }
	// รายปี
	public function insertStudentYear()
    {
		$courseOfferedId = $this->input->post('courseIDOld');
		$insertStuYearadd = $this->input->post('insertStuYearadd');
		$studentAll = $this->user_model->get_Student();
		// print_r($courseOfferedId);
		// echo exit;
		$courseOffered = $this->course_model->get_courseOfferedId($courseOfferedId);
		$result = [];
		$studentUserId = [];
		$n = 0;
		for ($i=0; $i < sizeOf($studentAll) ; $i++) { 
			if (strpos($studentAll[$i]['studentId'], $insertStuYearadd) === 0) {
				$studentUserId[$n] = $studentAll[$i]['userId'];
				$n++;
			}
		}
		// print_r($courseOffered);
		// echo exit;
		$result = array_merge($courseOffered[0]['studentId'], $studentUserId);
		$result = array_unique($result);
		$result = array_values($result);
		$data = array(
			"studentId" => (array)$result,
		);
		$this->course_model->insertProandStu_detailSubject($data,$courseOfferedId);
	
        redirect(base_url('detailSubject/showDetail/'.$courseOfferedId.''));
    }

	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$professor = $this->user_model->get_Professor();
		$student = $this->user_model->get_Student();
		$data['professor'] = $professor;
		$data['student'] = $student;
		$data['user'] = $user;
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('detailSubject/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}