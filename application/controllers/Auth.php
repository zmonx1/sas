<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('session');
    }
	public function login()
	{
		// $condition =[];
		$username = $this->input->post('email');
		$password = $this->input->post('password');
        $val = $this->user_model->get_user($username);
        $match = $val[0]['password'];
        // var_dump($match);

        if (!empty($val)) {
            // $match = $val->password; //Get password for user from database
            if ($match == $password) { //Condition if password matched
           
                $sess_array = array(
                    'id' => $val[0]['_id'],
                    'username' => $val[0]['email'],
                    'role' => $val[0]['role'],
                    'userId'=>$val[0]['userId'],
                    'isLoggedIn' => true,
                );
    
                $this->session->set_userdata($sess_array);
    
                // $output['status'] = true;
                // $this->response($output);
                redirect(base_url('dashboard'),'refresh');
            } else {
                // $output['status'] = false;
                // $output['message'] = 'รหัสผ่านไม่ถูกต้อง';
                // $this->response($output);
                redirect(base_url('login'),'refresh');

            }
        }else{
            redirect(base_url('login'),'refresh');
        }

	}
    public function logout()
	{
		$this->session->sess_destroy();
        redirect(base_url('login'),'refresh');
	}
}