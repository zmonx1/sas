<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AcademicianManagement extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
    }
	public function index()
	{
		$academician = $this->user_model->get_Academician();
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$data['user'] = $user;
		$data['academician'] = $academician;


		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('academicianManagement/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');

	}

	public function updateAcademician()
    {
		$adminEmailCheck = $this->input->post('adminEmailCheck');
		$prefixAdmin = $this->input->post('PrefixAdmin');
        $firstNameAdmin = $this->input->post('firstNameAdmin');
		$lastNameAdmin = $this->input->post('lastNameAdmin');
		$emailAdmin = $this->input->post('emailAdmin');
		

		$data = array(
			"prefix" => $prefixAdmin,
			"firstName" => $firstNameAdmin,
			"lastName" => $lastNameAdmin,
			"email" => $emailAdmin
		);
		// print_r($data);
		// echo exit;
		$this->user_model->updateAcademician($data,$adminEmailCheck);   
        redirect(base_url('AcademicianManagement'));
	
	}

	public function deleteAcademician()
    {	
		$adminPrefix= $this->input->post('prefix');
		$adminFirstName = $this->input->post('firstName');
		$adminLastName = $this->input->post('lastName');
		$adminEmail = $this->input->post('email');
		$adminPass = $this->input->post('password');
		$adminRole = $this->input->post('role');
		$adminUserID = $this->input->post('userId');
		$data = array(
			"prefix" => $adminPrefix,
			"firstName" => $adminFirstName,
			"lastName" => $adminLastName,
			"email" => $adminEmail,
			"password" => $adminPass,
			"role" => $adminRole,
			"userId" => $adminUserID,
		);
        $this->user_model->delAcademician($data);
		redirect(base_url('AcademicianManagement'));
    }
	public function getRandomHex($num_bytes=4) {
		return bin2hex(openssl_random_pseudo_bytes($num_bytes));
	}
	public function insertAcademician()
    {
		$adminPrefix = $this->input->post('adminPrefix-add-modal');
        $adminFirstName = $this->input->post('adminFirstName-add-modal');
		$adminLastName = $this->input->post('adminLastName-add-modal');
        $adminEmail = $this->input->post('adminEmail-add-modal');
		$adminPassword = "Swe_001";
		$adminRole = "1";
		$userId = $this->getRandomHex(8);

        $user = $this->user_model->get_user($adminEmail);?>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (นักวิชาการ)</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body prompt">
					<p id="adminNameShow"></p>
					</div>
					<div class="modal-footer prompt">
						<a href="javascript:history.back()" class="btn btn-primary">ตกลง</a>
					</div>
				</div>
			</div>
		</div>
	<?php
		if(empty($user)){
			$data2 = array(
				"prefix" => $adminPrefix,
				"firstName" => $adminFirstName,
				"lastName" => $adminLastName,
				"email" => $adminEmail,
				"password" => $adminPassword,
				"role" => $adminRole,
				"userId" => $userId,
			);
			$this->user_model->insertAlluser($data2);
			?>
			<script type="text/javascript">
				$('#adminNameShow').html("บันทึกข้อมูลนักวิชาการสำเร็จ");
				$("#modalSuccess").modal("show");
			</script>
		<?php }
		else{
			?>
			<script type="text/javascript">
				$('#adminNameShow').html("บันทึกข้อมูลนักวิชาการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
				$("#modalSuccess").modal("show");
			</script>
			<?php
			
		} 
		
    }
}