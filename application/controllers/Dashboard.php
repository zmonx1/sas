<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends BD_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('checkclass_model');
		$this->load->model('attendance_model');
	}
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$courseOffered = $this->course_model->findAllCourseOffered();
		$academician = $this->user_model->get_Academician();
		$student = $this->user_model->get_Student();
		$professor = $this->user_model->get_Professor();
		$course = $this->course_model->findAllCourse();
		// print_r($userId);
		// echo exit;
		$countCourse = 0;
		for ($i = 0; $i < sizeOf($courseOffered); $i++) {
			for ($j = 0; $j < sizeOf($courseOffered[$i]['teacherId']); $j++) {
				if ($user[0]['userId'] == $courseOffered[$i]['teacherId'][$j]) {
					$countCourse++;
				}
			}
		}
		$countStudy = 0;
		for ($i = 0; $i < sizeOf($courseOffered); $i++) {
			for ($j = 0; $j < sizeOf($courseOffered[$i]['studentId']); $j++) {
				// print_r($courseOffered[$i]['studentId'][$j]);
				// echo exit; 
				if ($user[0]['userId'] == $courseOffered[$i]['studentId'][$j]) {
					$countStudy++;
				}
			}
		}
		$courseOfferedEnroll = [];
		$courseOfferedEnrollPro = [];
		$courseEnroll = [];

		$courseID = [];
		$checkClass = [];
		$attendance = [];
		$countStudent = [];
		$numCourse = 0;
		$numCoursePro = 0;

		$countAttendance = [];
		// ----student---------
		for ($i = 0; $i < sizeof($courseOffered); $i++) {
			if (in_array($user[0]['userId'], ($courseOffered[$i]['studentId']))) {

				$courseOfferedEnroll[$numCourse] = $courseOffered[$i];
				$numCourse++;
			}
		}
		// -----professor-----------
		for ($i = 0; $i < sizeof($courseOffered); $i++) {
			if (in_array($user[0]['userId'], ($courseOffered[$i]['teacherId']))) {

				$courseOfferedEnrollPro[$numCoursePro] = $courseOffered[$i];
				$numCoursePro++;
			}
		}
		// print_r($courseOfferedEnroll);
		// echo exit;
		if($user[0]['role'] == 2){
			for ($i = 0; $i < sizeOf($courseOfferedEnroll); $i++) {
				$courseEnroll[$i] = $this->course_model->get_course($courseOfferedEnroll[$i]['courseId']);
				$checkClass[$i] = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedEnroll[$i]['courseOfferedId']);
			}
		}else{
			for ($i = 0; $i < sizeOf($courseOfferedEnrollPro); $i++) {
				$courseEnroll[$i] = $this->course_model->get_course($courseOfferedEnrollPro[$i]['courseId']);
				$checkClass[$i] = $this->checkclass_model->getClassByCourseOfferedId($courseOfferedEnrollPro[$i]['courseOfferedId']);
			}
		}
		
		for ($i = 0; $i < sizeOf($courseEnroll); $i++) {
			$courseID[] = $courseEnroll[$i][0]['courseID'];
		}
		// print_r($courseOfferedEnrollPro);
		// echo exit;
		for ($i=0; $i < sizeOf($courseOfferedEnrollPro); $i++) { 
			$countStudent[$i] = count($courseOfferedEnrollPro[$i]['studentId']);
		}
		// print_r($countStudent);
		// echo exit;

		for ($i = 0; $i < sizeOf($checkClass); $i++) {
			for ($j = 0; $j < sizeOf($checkClass[$i]); $j++) {
				$attendance[$i][$j] = $this->attendance_model->getAttendanceByCheckClassId($checkClass[$i][$j]['checkClassId']);
			}
		}
		// print_r($attendance);
		// echo exit;
		$countStudentAttendance = 0;
		$countStudentAttendanceArr = [];
		$periodArr = [];
		$graphPro = [];
		$graphProAbsent = [];

		


		if($user[0]['role'] == 2){
			for ($i = 0; $i < sizeOf($attendance); $i++) {
				if(!empty($attendance[$i])){
					for ($j=0; $j < sizeOf($attendance[$i]); $j++) { 
						if(!empty($attendance[$i][$j])){
							if (in_array($user[0]['studentId'],$attendance[$i][$j][0]['studentId'])) {
								$countAttendance[$i][$j] = 1;
							}
							else{
								$countAttendance[$i][$j] = 0;
							}
						}
						else{
							$countAttendance[$i][$j] = 0;
						}
					}
				}
			}
			$attendanceCheck = [];
			$periodCheck = [];
			$absentCheck = [];
			$count =0;
			// print_r($countAttendance);
			// echo exit;
			for ($i=0; $i < sizeOf($countAttendance); $i++) { 
				for ($j=0; $j < sizeOf($countAttendance[$i]); $j++) { 
					if($countAttendance[$i][$j] == 1){
						$count++;
					}
				}
				$attendanceCheck[$i] = $count;
				$count = 0;
	
			}
			// print_r($attendanceCheck);
			// echo exit;
			for ($i=0; $i < sizeOf($checkClass); $i++) { 
				$periodCheck[] =  count($checkClass[$i]);
			}
			for ($i=0; $i < sizeOf($periodCheck); $i++) { 
				if(!empty($attendanceCheck[$i])){
					$absentCheck[] = $periodCheck[$i]-$attendanceCheck[$i];
				}else{
					$absentCheck[] = $periodCheck[$i];
				}
			}
		$data['attendanceCheck'] = $attendanceCheck;
		$data['absentCheck'] = $absentCheck;

		}else{
			for ($i = 0; $i < sizeOf($checkClass); $i++) {
				if(!empty($attendance[$i])){
					for ($j=0; $j < sizeOf($attendance[$i]); $j++) { 
						if(!empty($attendance[$i][$j])){
							$countStudentAttendance += count($attendance[$i][$j][0]['studentId']);
						}
					}
					$countStudentAttendanceArr[$i] = $countStudentAttendance;
					$countStudentAttendance = 0;
				}else{
					$countStudentAttendanceArr[$i] = 0;
				}

			}
			// print_r($countStudentAttendanceArr);
			// echo exit;
			for ($i=0; $i < sizeOf($checkClass); $i++) { 
				$periodArr[$i] = count($checkClass[$i]);
			}
		
			for ($i=0; $i < sizeOf($periodArr); $i++) { 
				if($periodArr[$i]!=0){
					$graphPro[$i] = (($countStudentAttendanceArr[$i]/$countStudent[$i])/$periodArr[$i])*100;
					$graphProAbsent[$i] = 100-$graphPro[$i];
				}else{
					$graphPro[$i] = 0;
					$graphProAbsent[$i] = 100-$graphPro[$i];

				}
			}
			

			// print_r($graphProAbsent);
			// echo exit;


			
		}
		
		$data['graphProAbsent'] =$graphProAbsent;
		$data['graphPro'] =$graphPro;
		$data['courseID'] = $courseID;
		$data['countCourse'] = $countCourse;
		$data['user'] = $user;
		$data['academician'] = $academician;
		$data['student'] = $student;
		$data['professor'] = $professor;
		$data['course'] = $course;
		$data['countStudy'] = $countStudy;
		// print_r($countStudy);
		// echo exit;
		$this->load->view('layout/head');
		$this->load->view('layout/header', $data);
		$this->load->view('dashboard/content');
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');
	}
}
