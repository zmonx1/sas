<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileAndRePassword extends BD_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->isLoggedIn();
		$this->load->model('user_model');
    }
	public function index()
	{
		$username = $this->username;
		$user = $this->user_model->get_user($username);
		$userType = $this->user_model->get_userType($user[0]['role']);

		$data =  [];
		$data['user'] = $user;
		$data['userType'] = $userType;
		
		$this->load->view('layout/head');
		$this->load->view('layout/header',$data);
		$this->load->view('profileAndRePassword/content',$data);
		$this->load->view('layout/foot');
		$this->load->view('layout/footer');

	}
}
