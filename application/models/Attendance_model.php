<?php
defined('BASEPATH') or exit('No direct script access allowed');

class attendance_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('mongo_db');
    $this->load->helper('url');
  }
  public function insertAttendance($data)
  {
      $insertId = $this->mongo_db->insert('attendance', $data);
      return $insertId;
  }
  public function findAllAttendance($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('imageStudent');
    return $result;
  }
  public function getAttendanceByCheckClassId($checkClassId)
  {
    $filter = ['checkClassId' => ($checkClassId)];
    if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('attendance');
    return $result;
  }
  public function getAttendanceByCheckClassIdAll($checkClassId)
  {
    $filter = ['checkClassId' => ($checkClassId)];
    if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('attendance');
    return $result;
  }
  function delStuAttendance($studentId,$checkClassId)
  {
    $data = array('studentId' => $studentId);
    $this->mongo_db->set($data);
    $this->mongo_db->where('checkClassId', $checkClassId);
    $this->mongo_db->update('attendance');

  }
  function insertStudent($data)
  {
    $this->mongo_db->set($data);
    $this->mongo_db->where('checkClassId', $data['checkClassId']);
    $result = $this->mongo_db->update('attendance');
    return $result;
  }
  function delStd($data)
  {
    $this->mongo_db->where('checkClassId',$data['checkClassId']);
    $this->mongo_db->delete('attendance', array('checkClassId' => $data));
    // redirect(base_url('SubjectManagement'));
  }
}