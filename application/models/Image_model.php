<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Image_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('mongo_db');
    $this->load->helper('url');
  }
  public function insertImage($data)
  {
      $insertId = $this->mongo_db->insert('imageStudent', $data);
      return $insertId;
  }
  public function findAllStudentImage($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('imageStudent');
    return $result;
  }
  function get_imageByStudentId($studentId) {

    $filter = ['studentId' => ($studentId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('imageStudent');
    return $result;
  }
}