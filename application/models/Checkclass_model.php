<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkclass_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('mongo_db');
    $this->load->helper('url');
  }
  public function getClassByCheckClassId($checkClassId)
  {
    $filter = ['checkClassId' => ($checkClassId)];
    if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('checkClass');
    return $result;
  }
  
  public function getClassByCourseOfferedId($courseOfferedId)
  {
    $filter = ['courseOfferedId' => ($courseOfferedId)];
    if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('checkClass');
    return $result;
  }
  public function insertCheckClass($data)
  {
    $insertId = $this->mongo_db->insert('checkClass', $data);
    return $insertId;
  }
  function delCourseOffered($data,$courseOfferedId)
  {
    $this->mongo_db->where('courseOfferedId', $courseOfferedId);
    $this->mongo_db->delete('checkClass', array('courseOfferedId' => $data));
  }
  public function findAllClass($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('checkClass');
    return $result;
  }

  function delCheckClass($data)
  {
   
    $this->mongo_db->where('checkClassId', $data['checkClassId']);
    $this->mongo_db->delete('checkClass',array('checkClassId'=>$data));
  }
  
  function updateStatus($data)
  {
    // print_r($data['checkClassId']);
    // echo exit;
    $this->mongo_db->set($data);
    $this->mongo_db->where('checkClassId', $data['checkClassId']);
    $this->mongo_db->update('checkClass');
  }
}