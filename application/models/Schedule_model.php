<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Schedule_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('mongo_db');
    $this->load->helper('url');
  }

  public function getDayByCourseOfferedId($courseOfferedId)
  {
    $filter = ['courseOfferedId' => ($courseOfferedId)];
    if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('schedule');
    return $result;
  }

}