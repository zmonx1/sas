<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

  function __construct() {
		parent::__construct();
		$this->load->library('mongo_db');
    $this->load->helper('url');
		// $this->conn = $this->mongo_db->getConn();
	}
  
  function get_user($username) {

			$filter = ['email' => ($username)];
      if (sizeof($filter) > 0) {
        $this->mongo_db->where($filter);	
      }
      $result = $this->mongo_db->getOne('user');
      return $result;
  }
  function check_stdId($id) {

    $filter = ['studentId' => ($id)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('user');
    return $result;
}

  function get_userDisplayName($userId) {

    $filter = ['userId' => ($userId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('user');
    return $result;
}

  function get_userType($role) {
    $filter = ['role' => $role];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('userType');
    
    // echo "<pre>";
    //   print_r($result);
    // echo " </pre>";
    //   exit;
    return $result;
  } 

  function Repassword($dataOld,$newpassword,$oldpassword)
  {
    
    if(($dataOld[0]['password']) == $oldpassword){
      $data = array('password' => $newpassword);
      $this->mongo_db->set($data);
      $this->mongo_db->where('email', $dataOld[0]['email']);
      $this->mongo_db->update('user');
      
      return "true";
    }
    else{
      return "false";
    }   
  }
  public function setRole($userTypeId)
  {
    $filter = ['_id' => ($userTypeId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('role');
    return $result;
  }

  function get_Professor() {

    $filter = ['role' => '3'];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('user');
    return $result;
  }

  function updateProfessor($data,$proEmailCheck)
  {
    $this->mongo_db->set($data);
    $this->mongo_db->where('email',$proEmailCheck);
    $this->mongo_db->update('user');

    return "true";
  }

  function get_Academician() {

    $filter = ['role' => '1'];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('user');
    return $result;
  }

  function updateAcademician($data,$adminEmailCheck)
  {
    
    $this->mongo_db->set($data);
    $this->mongo_db->where('email',$adminEmailCheck);
    $this->mongo_db->update('user');

    return "true";
  }

  function get_Student() {

    $filter = ['role' => '2'];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('user');
    return $result;
  }

  function updateStudent($data,$StuIdCheck)
  {
    $this->mongo_db->set($data);
    $this->mongo_db->where('studentId',$StuIdCheck);
    $this->mongo_db->update('user');

    return "true";
  }

  function get_StudentEnroll($userId) {

    $filter = ['userId' => $userId];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('user');
    return $result;
  }
  function get_StudentAttendance($studentId) {

    $filter = ['studentId' => $studentId];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('user');
    return $result;
  }

  function delStuEnroll($studentId,$courseOfferedID)
  {
    $data = array('studentId' => (array)$studentId);
    $this->mongo_db->set($data);
    $this->mongo_db->where('courseOfferedId', $courseOfferedID);
    $this->mongo_db->update('coursesOffered');

    // print_r($data);
    // echo exit;
    redirect(base_url('detailSubject/showDetail/'.$courseOfferedID.''));
  }

  function delAcademician($data)
  {
    $this->mongo_db->where('email', $data['email']);
    $this->mongo_db->delete('user',array('email'=>$data));
    // redirect(base_url('ProfessorManagement'));
  }

  function delProfessor($data)
  {
    $this->mongo_db->where('email', $data['email']);
    $this->mongo_db->delete('user',array('email'=>$data));
    // redirect(base_url('ProfessorManagement'));
  }
  function delProEnroll($teacherId,$courseID)
  {
    $data = array('teacherId' => (array)$teacherId);
    $this->mongo_db->set($data);
    $this->mongo_db->where('courseId', $courseID);
    $this->mongo_db->update('coursesOffered');

    // print_r($data);
    // echo exit;
    // redirect(base_url('ProfessorManagement'));
  }
  function delStuEnroll2($studentId,$courseOfferedID)
  {
    $data = array('studentId' => (array)$studentId);
    $this->mongo_db->set($data);
    $this->mongo_db->where('courseOfferedId', $courseOfferedID);
    $this->mongo_db->update('coursesOffered');

    // print_r($data);
    // echo exit;
    // redirect(base_url('ProfessorManagement'));
  }
  

  function delStu($data)
  {
    $this->mongo_db->where('email', $data['email']);
    $this->mongo_db->delete('user',array('email'=>$data));
    // redirect(base_url('StudentManagement'));
  }

  function insertAlluser($data)
  {
      $insertId = $this->mongo_db->insert('user', $data);
      return $insertId;
  }


}