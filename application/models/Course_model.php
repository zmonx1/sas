<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('mongo_db');
    $this->load->helper('url');
  }


  // Can use insert //

  function insertSubject($data)
  {
    $insertId = $this->mongo_db->insert('course', $data);
    return $insertId;
  }

  public function findAllCourse($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('course');
    return $result;
  }


  function delSub($data, $cIDOld)
  {
    $this->mongo_db->where('courseID', $cIDOld);
    $this->mongo_db->delete('course', array('courseID' => $data));
    // redirect(base_url('SubjectManagement'));
  }
  function delCourseOffered($data)
  {
    $this->mongo_db->where('courseID', $data['courseID']);
    $this->mongo_db->delete('courseOffered', array('courseID' => $data));
    // redirect(base_url('SubjectManagement'));
  }

  function updateSub($data, $cIDOld)
  {
    // echo "<pre>";
    // print_r($data);
    // echo "</pre>";
    // echo exit;
    $this->mongo_db->set($data);
    $this->mongo_db->where('courseID', $cIDOld);
    $this->mongo_db->update('course');

    return "true";
  }
  //------------------- Course Offered------------------------------------

  public function getTerm($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('term');
    return $result;
  }
  function insertCourseOffered($data)
  {
    $insertId = $this->mongo_db->insert('coursesOffered', $data);
    return $insertId;
  }

  public function findAllCourseOffer($condition = [])
  {
    if (sizeof($condition) > 0) {
      $this->mongo_db->where($condition);
    }
    $result = $this->mongo_db->get('course');
    return $result;
  }
 

  function get_courseID($courseID) {

    $filter = ['courseID' => ($courseID)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('course');
    return $result;
  }
  function get_course($courseId) {

    $filter = ['courseId' => ($courseId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('course');
    return $result;
  }
  function get_courseByCourseID($courseID) {

    $filter = ['courseID' => ($courseID)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->getOne('course');
    return $result;
  }

  function get_courseOffered($courseId) {

    $filter = ['courseId' => ($courseId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('coursesOffered');
    return $result;
  }
  function get_courseOfferedId($courseOfferedId) {

    $filter = ['courseOfferedId' => ($courseOfferedId)];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('coursesOffered');
    return $result;
  }

  // function get_proCourseOffered($teacherId) {

  //   $filter = ['teacherId' => ($teacherId)];
  //   if (sizeof($filter) > 0) {
  //     $this->mongo_db->where($filter);	
  //   }
  //   $result = $this->mongo_db->getOne('coursesOffered');
  //   return $result;
  // }

  public function findAllCourseOffered()
  {
    $filter = [];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);
    }
    $result = $this->mongo_db->get('coursesOffered');
    return $result;
  }
  public function findAllCourseOfferedStatus()
  {
    $filter = ['status'=>'1'];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);
    }
    $result = $this->mongo_db->get('coursesOffered');
    return $result;
  }
  public function findCourseOffered($courseID)
  {
    $filter = ['courseId' => $courseID];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);
    }
    $result = $this->mongo_db->getOne('course');
    return $result;
  }
  function get_courseOfferedByProfessor($userId) {

    $filter = ['teacherId' => $userId];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('courseOffered');
    return $result;
}

  function updatecloseStatus($data, $courseIDOld)
  {
    // echo "<pre>";
    // print_r($data);
    // echo "</pre>";
    // echo exit;
    $this->mongo_db->set($data);
    $this->mongo_db->where('courseOfferedId', $courseIDOld);
    $this->mongo_db->update('coursesOffered');

    return "true";
  }

  function insertProandStu_detailSubject($data,$courseIDOld)
  {
    {
      
      $this->mongo_db->set($data);
      $this->mongo_db->where('courseOfferedId', $courseIDOld);
      $this->mongo_db->update('coursesOffered');
  
      return "true";
    }
  }
  //----------------------------- schedule ----------------------
  public function insertScheduleToTeach($data)
  {
      $insertId = $this->mongo_db->insert('schedule', $data);
      return $insertId;
  }

  function get_ScheduleToTeach($courseOfferedId) {

    $filter = ['courseOfferedId' => $courseOfferedId];
    if (sizeof($filter) > 0) {
      $this->mongo_db->where($filter);	
    }
    $result = $this->mongo_db->get('schedule');
    return $result;
  }

}
