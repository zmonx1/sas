<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login &mdash; Stisla</title>
  <!-- Font CSS  -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/bootstrap/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/fontawesome/css/all.min.css') ?>">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/bootstrap-social/bootstrap-social.css') ?>">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/components.css') ?>">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand prompt">
              <img src="assets/img/banner.png" alt="logo" width="100%" height="100%">
            </div>

            <div class="card card-primary ">
              <div class="card-header prompt">
                <h4>เข้าสู่ระบบ</h4>
              </div>

              <div class="card-body prompt">
                <form method="POST" action="<?php echo base_url(); ?>auth/login" class="needs-validation" novalidate="" id="frm-login">

                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                    </div>
                    <input name="email" type="email" class="input form-control" id="email" placeholder="Email" aria-label="email" aria-describedby="basic-addon1" tabindex="1" required autofocus />
                    <div class="invalid-feedback">
                      กรุณากรอก Email
                    </div>
                  </div>

                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                    </div>
                    <input name="password" type="password" value="" class="input form-control" id="password" placeholder="Password" required="true" aria-label="password" aria-describedby="basic-addon1" tabindex="2" />
                    <div class="input-group-append">
                      <span class="input-group-text" onclick="password_show_hide();">
                        <i class="fas fa-eye" id="show_eye"></i>
                        <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                      </span>
                    </div>
                    <div class="invalid-feedback">
                      กรุณากรอก Password
                    </div>
                  </div>

                  <div class="form-group">
                    <br>
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Login
                    </button>
                  </div>
                </form>

              </div>
            </div>

            <div class="simple-footer prompt">
              <p>พัฒนาโดย หลักสูตรวิศวกรรมซอฟต์แวร์ <br>สำนักวิชาวิศวกรรมศาสตร์และเทคโนโลยี มวล.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <style>
    .login_oueter {
      width: 360px;
      max-width: 100%;
    }

    .logo_outer {
      text-align: center;
    }

    .logo_outer img {
      width: 120px;
      margin-bottom: 40px;
    }
  </style>


  <!-- General JS Scripts -->
  <script src="<?php echo base_url('assets/modules/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/popper.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/tooltip.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/nicescroll/jquery.nicescroll.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/moment.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/stisla.js') ?>"></script>

  <!-- JS Libraies -->

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="<?php echo base_url('assets/js/scripts.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
  <script>
    function password_show_hide() {
      var x = document.getElementById("password");
      var show_eye = document.getElementById("show_eye");
      var hide_eye = document.getElementById("hide_eye");
      hide_eye.classList.remove("d-none");
      if (x.type === "password") {
        x.type = "text";
        show_eye.style.display = "none";
        hide_eye.style.display = "block";
      } else {
        x.type = "password";
        show_eye.style.display = "block";
        hide_eye.style.display = "none";
      }
    }
  </script>
</body>

</html>