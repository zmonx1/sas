<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>

<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>ยินดีต้อนรับ <?php echo $user[0]['prefix'] ?><?php echo $user[0]['firstName'] ?> <?php echo $user[0]['lastName'] ?></h4>
        </div>
    </section>

    <div class="row">
        <?php
        $role = $this->session->role;
        if ($role != '2') {
        ?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-calendar"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>วัน เดือน ปี</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php
                                $thaiweek = array("วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัส", "วันศุกร์", "วันเสาร์");
                                $thaimonth = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
                                echo $thaiweek[date("w")], " ที่ ", date("j "), $thaimonth[date("m") - 1], " พ.ศ. ", date("Y") + 543;
                                ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-swatchbook"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนรายวิชาที่สอน</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php echo $countCourse ?> วิชา</h5>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

        <?php
        $role = $this->session->role;
        if ($role == '2') {
        ?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-calendar"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>วัน เดือน ปี</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php
                                $thaiweek = array("วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัส", "วันศุกร์", "วันเสาร์");
                                $thaimonth = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
                                echo $thaiweek[date("w")], " ที่ ", date("j "), $thaimonth[date("m") - 1], " พ.ศ. ", date("Y") + 543;
                                ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-swatchbook"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนรายวิชาที่เรียน</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php echo $countStudy ?> วิชา</h5>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php
    $role = $this->session->role;
    if ($role == '1') {
    ?>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนอาจารย์ในระบบ</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php $n = 0;
                                echo count((array)$professor);
                                $n++ ?> คน</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-info">
                        <i class="fas fa-user-graduate"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนนักศึกษาในระบบ</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php $n = 0;
                                echo count((array)$student);
                                $n++ ?> คน</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนนักวิชาการในระบบ</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php $n = 0;
                                echo count((array)$academician);
                                $n++ ?> คน</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-swatchbook"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header prompt">
                            <h4>จำนวนรายวิชาในระบบ</h4>
                        </div>
                        <div class="card-wrap prompt">
                            <h5><?php $n = 0;
                                echo count((array)$course);
                                $n++ ?> วิชา</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <?php
        $role = $this->session->role;
        if ($role != '2') {
        ?>
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header prompt">
                        <h4>กราฟสรุปการเข้าชั้นเรียนออนไลน์ของนักศึกษา (รายวิชา)</h4>
                    </div>
                    <div class="card-body">
                        <div id="chartContainer" style="height: 300px; width: 100%;">
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            $role = $this->session->role;
            if ($role == '2') {
            ?>
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header prompt">
                            <h4>กราฟแสดงการเข้าชั้นเรียน</h4>
                        </div>
                        <div class="card-body">
                            <canvas id="myChart2" style="height: 400px; width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
    </div>
</div>

<script type="text/javascript">
    window.onload = function() {
        var chart = new CanvasJS.Chart("chartContainer", {
            toolTip: {
                shared: true
            },
            axisY: {
                title: "percent"
            },
            data: [{
                    type: "stackedBar100",
                    showInLegend: true,
                    name: "เข้าเรียน",
                    color: "#6777ef",
                    dataPoints: [
                        <?php for ($i = 0; $i < sizeOf($courseID); $i++) { ?> {
                                y: <?= $graphPro[$i] ?>,
                                label: "<?= $courseID[$i]  ?>"
                            },
                        <?php }  ?>

                    ]
                },
                {
                    type: "stackedBar100",
                    showInLegend: true,
                    name: "ไม่เข้าเรียน",
                    color: "#ffa426",
                    dataPoints: [
                        <?php for ($i = 0; $i < sizeOf($courseID); $i++) { ?> {
                                y: <?= $graphProAbsent[$i] ?>,
                                label: "<?= $courseID[$i]  ?>"
                            },
                        <?php }  ?>
                    ]
                }
            ]

        });

        chart.render();
    }
</script>

<script>
    var ctx = document.getElementById("myChart2").getContext("2d");

    var data = {

        labels: <?= json_encode($courseID) ?>,
        datasets: [{
                label: "เข้าเรียน",
                backgroundColor: "#6777ef",
                data: <?= json_encode($attendanceCheck) ?>
            },
            {
                label: "ไม่เข้าเรียน",
                backgroundColor: "#ffa426",
                data: <?= json_encode($absentCheck) ?>
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                    }
                }]
            }
        }
    });
</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>