<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>อัปโหลดรูปเพื่อการรู้จำใบหน้า</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/subjectManagement') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/uploadFaceStudent') ?>">อัปโหลดรูปใบหน้า</a>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card card-primary">
                <div class="card-header prompt">
                    <h4>อัปโหลดรูปใบหน้า</h4>
                    <div class="card-header-action">
                        <?php if($status == 1){?>
                            <div class="badge badge-success">บันทึกแล้ว</div>
                       <?php }else{?>
                            <div class="badge badge-danger">ยังไม่บันทึก</div>
                       <?php } ?>
                    </div>
                </div>
                <div class="card-body prompt">
                    <p>การอัปโหลดรูป ต้องใช้รูปภาพถ่ายใบหน้าอย่างน้อย 10 รูป <a class="text-danger">(รูปถ่ายใบหน้าตรง, รูปถ่ายเอียงข้างซ้าย, รูปถ่ายเอียงข้างขวา, รูปถ่ายหน้ายิ้ม)</a></p>
                    <form method="post" action="<?php echo base_url("/UploadFaceStudent/uploadFileStudent"); ?>" name="file"  class="dropzone"></form>
                </div>
                <div class="card-footer text-right prompt">
                    <input type="button" class="btn btn-primary" id="uploadStd" value='อัปโหลด'>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js' type='text/javascript'></script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone(".dropzone", { 
        url: '<?php echo base_url("/UploadFaceStudent/uploadFileStudent"); ?>',
        paramName: 'file',
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 20, // Number of files process at a time (default 2)
        addRemoveLinks: true,
        maxFile:20,
        maxFilesize: 20,   // MB
        dictRemoveFile:'remove',
        dicCancelUpload:'Cancel',
        dictFileTooBig: "ไม่อนุญาติให้อัพโหลดเกิน 20 MB",
        acceptedFiles: ".jpeg,.jpg,.png",  
        init:function() {
            
        },
        success: function(file,response) {
            console.log("response");
            console.log(response);
            window.location.replace("<?php echo base_url('/UploadFaceStudent') ?>");
        },
        complete:function(file) {
            console.log(file)
        }
        
    });
    $('#uploadStd').click(function(){
    myDropzone.processQueue();
    });

</script>