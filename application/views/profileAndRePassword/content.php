    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="far fa-user"></i> ข้อมูลส่วนตัว</h4>
                <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                        <a href="">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="">ข้อมูลส่วนตัว</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>ข้อมูลส่วนตัว</h4>
                        <div class="card-header-action">
                            <a href="<?php echo base_url('/rePassword')?>" class="btn btn-primary">
                                แก้ไขรหัสผ่าน
                            </a>
                        </div>
                    </div>
                    <div class="card-body prompt">
                        <div style="font-size: 16px;" class="form-row">
                            <?php 
                            $role = $this->session->role;
                            if ($role == '2') {
                            ?>
                            <div class="form-group col-md-6">
                                <label>รหัสนักศึกษา:</label>
                                <p><?php echo $user['0']['studentId'] ?></p>
                            </div>
                            <?php }?>
                            <div class="form-group col-md-6">
                                <label>ชื่อ - นามสกุล:</label>
                                <p><?php echo $user['0']['prefix'] ?><?php echo $user['0']['firstName'] ?> <?php echo $user['0']['lastName'] ?></p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>ตำแหน่ง:</label>
                                <p><?php echo $userType['0']['userTypeName'] ?></p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>อีเมล:</label>
                                <p><?php echo $user['0']['email'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title prompt">แก้ไขรหัสผ่าน</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <div class="form-group prompt">
                        <label>รหัสผ่าน (เดิม)</label>
                        <input type="password" class="form-control col-8" required placeholder="กรุณากรอกรหัสผ่านเดิม">
                    </div>
                    <div class="form-group prompt">
                        <label>รหัสผ่าน (ใหม่)</label>
                        <input type="password" class="form-control is-invalid col-8" required placeholder="กรุณากรอกรหัสผ่านใหม่">
                        <div class="invalid-feedback prompt">
                            รหัสผ่านควรประกอบไปด้วยตัวอักษร (a-z, A-Z) มีตัวเลข (0-9) <br>และมีเครื่องหมายหรืออักขระพิเศษ (!@#$%^&*()_+|~-=`{}[]:”;'<>?,./)
                        </div>
                    </div>
                    <div class="form-group prompt">
                        <label>ยืนยันรหัสผ่าน</label>
                        <input type="password" class="form-control is-invalid col-8" required placeholder="กรุณากรอกยืนยันรหัสผ่าน">
                        <div class="invalid-feedback prompt">
                            รหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง
                        </div>
                    </div>
                </div>
                <div class="modal-footer prompt">
                    <button type="button" class="btn btn-primary">ยืนยันรหัสผ่าน</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>