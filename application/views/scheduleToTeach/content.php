    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-tasks"></i> กำหนดเปิดสอน</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/scheduleToTeach') ?>">กำหนดเปิดสอน</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormTeach" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>จัดการรายวิชาที่เปิดสอน</h4>
                        </div>
                        <div class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label>ภาคการศึกษา*</label>
                                    <select class="form-control" id="term-add" name="term-add" required>
                                        <option value="" disabled selected>เลือกภาคการศึกษา</option>
                                        <?php
                                        foreach ($term as $row) : ?>
                                            <option value="<?php echo $row['termNo'] ?>"><?php echo $row['termNo'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <?php ?>
                                    <label>ปีการศึกษา*</label>
                                    <select class="form-control" id="year-add" name="year-add" required>
                                        <option value="" disabled selected>เลือกปีการศึกษา</option>
                                        <?PHP for ($i = 0; $i <= 50; $i++) { ?>
                                            <option value="<?PHP echo date("Y") - $i + 543 ?>"><?PHP echo date("Y") - $i + 543 ?></option>
                                        <?PHP } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>วันที่เริ่มต้น*</label>
                                    <!-- <input class="input-medium" type="text" data-provide="datepicker" data-date-language="th-th"> -->
                                    <input type="text" class="form-control" name="startDate" id= "startDate" data-provide="datepicker" data-date-language="th-th" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>วันที่สิ้นสุด*</label>
                                    <input type="text" class="form-control" name="endDate" id= "endDate" data-provide="datepicker" data-date-language="th-th" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>รายวิชาที่เปิดสอน</label>
                                    <select class="form-control selectric" id="courseID-add" name="courseOffer[]" multiple="multiple" required>
                                        <option disabled selected value="">เลือกรายวิชาที่เปิดสอน</option>
                                        <?php
                                        foreach ($course as $row) : ?>
                                            <option value="<?php echo $row['courseID'] ?>"><?php echo $row['courseID'] ?> <?php echo $row['courseNameTH'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <button class="btn btn-primary trigger--fire-modal-1" id="addTeach">ยืนยัน</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายวิชาที่เปิดสอน</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th class="col-2">ปีการศึกษา</th>
                                        <th class="col-4">วิชา</th>
                                        <th class="col-2">อาจารย์ผู้สอน</th>
                                        <th class="col-2">จำนวน นศ.</br>(คน)</th>
                                        <th class="col-2"></th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <?php
                                    $no = 1;
                                    $i = 0;
                                    $n = 0;
                                    $j = 0;
                                    $count = 0;
                                    $displayName = [];
                                    foreach ($courseOfferedName as $row) : ?>
                                        <tr style="font-size: 14px;">
                                            <td style="text-align: center;"><?php echo $courseOffered[$i]['term']; ?>/<?php echo $courseOffered[$i]['year']; ?></td>
                                            <td><b><?php echo $row[0]['courseID'] ?></b><br><?php echo $row[0]['courseNameTH'] ?><br> (<?php echo $row[0]['courseNameEN'] ?>)</td>
                                            <?php if (empty($courseOfferedProfessorName[$i])) {
                                                $i++ ?>
                                                <td style="text-align: center"><a class="text-danger">ไม่มีข้อมูล</a></td>
                                            <?php } else { ?>
                                                <td style=""><a class="text"><?php
                                                for ($k=0; $k < sizeOf($courseOfferedProfessorName[$i]); $k++) { 
                                                    echo $courseOfferedProfessorPrefixName[$i][$k].$courseOfferedProfessorName[$i][$k]." ".$courseOfferedProfessorLastName[$i][$k]."</br>";
                                                }
                                                $i++ ?></a></td>
                                            <?php } ?>
                                            <td style="text-align: center"><a class="text"><?php echo count($courseOffered[$n]['studentId']);?></a></td>
                                            <td class="text-center">
                                                <a href="<?php echo base_url('/detailSubject/showDetail') ?>/<?= $courseOffered[$n]['courseOfferedId'] ?>"><button class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                                                <?php $n++?>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#fire-modal-2<?php echo $courseOffered[$count]['courseOfferedId']; $count++ ?>"><i class="fas fa-times"></i></button>
                                            </td>
                                            <?php ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('ScheduleToTeach/courseOffered'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">จัดการรายวิชาที่เปิดสอน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <input type="hidden" id="term-add-modal" name="term-add-modal" value="" />
                        <input type="hidden" id="year-add-modal" name="year-add-modal" value="" />
                        <input type="hidden" id="courseID-add-modal" name="courseID-add-modal" value="" />
                        <input type="hidden" id="startDate-add" name="startDate-add" value="" />
                        <input type="hidden" id="endDate-add" name="endDate-add" value="" />
                        <p id="subjectshow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <?php $no = 0; $count=0; $i=0;
    foreach ($courseOfferedName as $row) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-2<?php echo $courseOffered[$count]['courseOfferedId']; ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('ScheduleToTeach/closeCourseOffered'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ปิดรายวิชา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_fire_modal_2('<?php echo $courseOffered[$i]['courseOfferedId'] ?>')">&times;</span>
                            </button>
                        </div>
                        <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $courseOffered[$count]['courseOfferedId']; $count++ ?>" />
                        <input type="hidden" id="courseStatus" name="courseStatus" value="0" />
                        <div class="modal-body prompt">
                            <p>ต้องการปิดรายวิชา <b><?php echo $row[0]['courseID'] ?> <?php echo $row[0]['courseNameTH'] ?> <?php echo $row[0]['courseNameEN'] ?></b> หรือไม่</p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_fire_modal_2('<?php echo $courseOffered[$i]['courseOfferedId']; $i++ ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>
    <script>
        $('#addFormTeach').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var courseID = $("#courseID-add").val();
            // var courseArray = courseID.split(',');
            var term = $("#term-add").val();
            var year = $("#year-add").val();
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();

            if ((!!courseID) && (!!term) && (!!year) && (!!startDate) && (!!endDate)) {
                $('#fire-modal-1').modal('show');
                console.log("modal")
                var str = "<b>ภาคการศึกษา:</b> " + term + "/" + year + "<br><b>วิชาที่เปิดสอน:</b> " + courseID ;
                $('#subjectshow').html(str);
                $('#term-add-modal').val(term);
                $('#year-add-modal').val(year);
                $('#courseID-add-modal').val(courseID);
                $('#startDate-add').val(startDate);
                $('#endDate-add').val(endDate);

            }
        });

        xmodal = () => {
            $('#fire-modal-1').modal('hide');
            $('.modal-backdrop').remove();
        }

        x_fire_modal_2 = (i) => {
            $('#fire-modal-2' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>
    <script>
       function demo() {
        $('#startDate').datepicker();
        $('#endDate').datepicker();
      }
    </script>