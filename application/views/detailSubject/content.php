    <!-- Main Content -->
    <?php $day = ['วันอาทิตย์', 'วันจันทร์', 'วันอังคาร','วันพุธ','วันพฤหัสบดี','วันศุกร์','วันเสาร์'] ?>
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-tasks"></i> จัดการรายวิชาที่เปิดสอน</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/scheduleToTeach') ?>">จัดการรายวิชาที่เปิดสอน</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/detailSubject') ?>">บันทึกข้อมูลรายวิชา</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>บันทึกข้อมูลรายวิชา</h4>
                    </div>
                    <div style="font-size: 16px;" class="card-body prompt">
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label>รหัสวิชา</label>
                                <p><?php echo $course[0]['courseID'] ?></p>
                            </div>
                            <div class="form-group col-md-5">
                                <label>ชื่อวิชา (ภาษาไทย)</label>
                                <p><?php echo $course[0]['courseNameTH'] ?></p>
                            </div>
                            <div class="form-group col-md-5">
                                <label>ชื่อวิชา (ภาษาอังกฤษ)</label>
                                <p><?php echo $course[0]['courseNameEN'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormStartTeach" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>บันทึกข้อมูลการจัดการเรียนการสอน</h4>
                        </div>
                        <div style="font-size: 16px;" class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>วัน</label>
                                    <select class="form-control" id="dayTeach" name="dayTeach" required>
                                        <option value=""  disabled selected>เลือกวันที่จัดการเรียนการสอน</option>
                                        <option value="Sunday">อาทิตย์</option>
                                        <option value="Monday">จันทร์</option>
                                        <option value="Tuesday">อังคาร</option>
                                        <option value="Wednesday">พุธ</option>
                                        <option value="Thursday">พฤหัสบดี</option>
                                        <option value="Friday">ศุกร์</option>
                                        <option value="Saturday">เสาร์</option>
                                    </select>
                                    <div class="invalid-feedback">กรุณาเลือกวันที่จัดการเรียนการสอน</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>เวลาเริ่มต้น</label>
                                    <input type="time" class="form-control" name="startTime" id="startTime" required>
                                    <div class="invalid-feedback">กรุณาเลือกเวลาเริ่มต้น</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>เวลาสิ้นสุด</label>
                                    <input type="time" class="form-control" name="endTime" id="endTime" required>
                                    <div class="invalid-feedback">กรุณาเลือกเวลาสิ้นสุด</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <td style="text-align: center;"><button class="btn btn-primary trigger--fire-modal-2" id="SaveSchedule">บันทึก</button></td>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>ตารางข้อมูลการจัดการเรียนการสอน</h4>
                    </div>
                    <div style="font-size: 16px;" class="card-body prompt">
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-striped" id="sortable-table">
                                    <thead>
                                        <tr style="font-size: 14px;" class="text-center">
                                            <th class="col-1">คาบที่</th>
                                            <th class="col-3">วัน-เดือน-ปี</th>
                                            <th class="col-2">วัน</th>
                                            <th class="col-2">เวลาเริ่มต้น</th>
                                            <th class="col-2">เวลาสิ้นสุด</th>
                                            <th class="col-2"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="ui-sortable">
                                    <?php $no = 1; 
                                        foreach ($checkClass as $teach) : ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $no++ ?></td>
                                            <td style="text-align: center;"><?php echo $teach['date'] ?></td>
                                            <?php if ($teach['day'] == 'Sunday') {
                                            ?>
                                                <td style="text-align: center">อาทิตย์</td>
                                            <?php } else if($teach['day'] == 'Monday') { 
                                            ?>
                                                <td style="text-align: center">จันทร์</td>
                                            <?php } else if($teach['day'] == 'Tuesday') { 
                                            ?>
                                                <td style="text-align: center; ">อังคาร</td>
                                            <?php } else if($teach['day'] == 'Wednesday') { 
                                            ?>
                                                <td style="text-align: center">พุธ</td>
                                            <?php } else if($teach['day'] == 'Thursday') { 
                                            ?>
                                                <td style="text-align: center">พฤหัสบดี</td>
                                            <?php } else if($teach['day'] == 'Friday') { 
                                            ?>
                                                <td style="text-align: center">ศุกร์</td>
                                            <?php } else if($teach['day'] == 'Saturday') { 
                                            ?>
                                                <td style="text-align: center">เสาร์</td>
                                            <?php } ?>
                                            <td style="text-align: center;"><?php echo $teach['startTime'] ?> น.</td>
                                            <td style="text-align: center;"><?php echo $teach['endTime'] ?> น.</td>
                                            <td style="text-align: center;"><button class="btn btn-danger trigger--fire-modal-3" id="modal-3"><i class="fas fa-trash"></i></button></td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        <div class="col-6 col-md-6 col-lg-6">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>เพิ่มอาจารย์ผู้สอน</h4>
                    </div>
                    <div style="font-size: 16px;" class="card-body prompt">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>เพิ่มอาจารย์ผู้สอน</label>
                                <select name="insertProf" id="insertProf" class="form-control selectric" multiple="">\
                                    <option disabled>เลือกรายชื่ออาจารย์</option>
                                    <?php
                                    for ($i = 0; $i < count($professor); $i++) {
                                        $selected = (in_array($professor[$i]['userId'], $courseOfferedProfessor) ? 'selected="selected"' : '');
                                        echo "<option value='" . $professor[$i]['userId'] . "' $selected>" . $professor[$i]['prefix'] . $professor[$i]['firstName'] . '    ' . $professor[$i]['lastName'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right prompt">
                        <button class="btn btn-primary" onclick="getInsertPro()" data-toggle="modal" data-target="#insertProfessor<?php echo $course[0]['courseId'] ?>">บันทึก</button>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-6 col-lg-6">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>เพิ่มรายชื่อนักศึกษา (รหัสนักศึกษา)</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>รหัสนักศึกษา (ขึ้นต้นด้วย)</label>
                                <select name="insertStuYear" id="insertStuYear" class="form-control">
                                    <option disabled selected>เลือกรหัสนักศึกษา (ขึ้นต้นด้วย)</option>
                                    <?php foreach ($studentId as $role) : ?>
                                        <option value="<?php echo $role ?>"><?php echo $role ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right prompt">
                        <td style="text-align: center;"><button onclick="getInsertStuYear()" class="btn btn-primary" data-toggle="modal" data-target="#insertStudentYear<?php echo $course[0]['courseId'] ?>">บันทึก</button></td>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>เพิ่มรายชื่อนักศึกษา (รายบุคคล)</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>รายชื่อนักศึกษา</label>
                                <select name="insertStu" id="insertStu" class="form-control select2 prompt" multiple="">
                                    <?php foreach ($student as $role) : ?>
                                        <option value="<?php echo $role['userId'] ?>"><?php echo $role['studentId'] ?> <?php echo $role['prefix'] ?><?php echo $role['firstName'] ?> <?php echo $role['lastName'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right prompt">
                        <button class="btn btn-primary" onclick="getInsertStu()" data-toggle="modal" data-target="#insertStudent<?php echo $course[0]['courseId'] ?>">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายชื่อนักศึกษาที่ลงทะเบียนเรียนในรายวิชานี้</h4>
                    </div>
                    <div style="font-size: 16px;" class="card-body prompt">
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-striped" id="sortable-table" >
                                    <thead>
                                        <tr style="font-size: 14px;" class="text-center">
                                            <th class="col-2">ลำดับที่</th>
                                            <th class="col-2">รหัสนักศึกษา</th>
                                            <th class="col-2">ชื่อ-สกุล</th>
                                            <th class="col-3">อีเมล</th>
                                            <th class="col-2"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="ui-sortable" id="sortable-table">
                                        <?php $no = 1;
                                        foreach ($studentEnroll as $stu) : ?>
                                            <tr style="font-size: 14px;">
                                                <td style="text-align: center;"><?php echo $no++ ?></td>
                                                <td><?php echo $stu[0]['studentId'] ?></td>
                                                <td><?php echo $stu[0]['prefix'] ?><?php echo $stu[0]['firstName'] ?> <?php echo $stu[0]['lastName'] ?></td>
                                                <td><?php echo $stu[0]['email'] ?></td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#delModalStuEnroll<?php echo $stu[0]['studentId'] ?>"><i class="fas fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertProfessor<?php echo $course[0]['courseId'] ?>">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('detailSubject/insertProfessor'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มอาจารย์ผู้สอน <?php echo $course[0]['courseID'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $courseOffered[0]['courseOfferedId'] ?>" />
                        <input type="hidden" id="insertProadd" name="insertProadd" value="" />
                        <p id="selectNameShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertStudent<?php echo $course[0]['courseId'] ?>">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('detailSubject/insertStudent'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มรายชื่อนักศึกษา (รายบุคคล)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $courseOffered[0]['courseOfferedId'] ?>" />
                        <input type="hidden" id="insertStuadd" name="insertStuadd" value="" />
                        <p id="selectNameStuShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php $no = 1;
    foreach ($studentEnroll as $stu) : ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="delModalStuEnroll<?php echo $stu[0]['studentId'] ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('detailSubject/showDetail') ?>/<?= $courseOffered[0]['courseOfferedId'] ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบรายชื่อนักศึกษา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_StuEnroll_modal(<?php echo $stu[0]['studentId'] ?>)">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body prompt">
                            <input type="hidden" id="emailOld" name="emailOld" type="text" class="form-control" value="<?php echo $stu[0]['email'] ?>">
                            <input type="hidden" id="studentUserId" name="studentUserId" type="text" class="form-control" value="<?php echo $stu[0]['userId'] ?>">

                            <p>ต้องการลบ <b><?php echo $stu[0]['prefix'] ?><?php echo $stu[0]['firstName'] ?> <?php echo $stu[0]['lastName'] ?></b> หรือไม่</p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_StuEnroll_modal(<?php echo $stu[0]['studentId'] ?>)">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    
    <div class="modal fade" tabindex="-1" role="dialog" id="insertStudentYear<?php echo $course[0]['courseId'] ?>">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('detailSubject/insertStudentYear'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มรายชื่อนักศึกษา (รหัสนักศึกษา)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $courseOffered[0]['courseOfferedId'] ?>" />
                        <input type="hidden" id="insertStuYearadd" name="insertStuYearadd" value="" />
                        <p id="selectNameStuYearShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-2">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('detailSubject/insertSchedule'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">บันทึกข้อมูลการจัดการเรียนการสอน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                            <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $courseOffered[0]['courseOfferedId'] ?>" />
                            <input type="hidden" id="dayTeach-add" name="dayTeach-add" value="" />
                            <input type="hidden" id="startTime-add" name="startTime-add" value="" />
                            <input type="hidden" id="endTime-add" name="endTime-add" value="" />

                            <p id="detailshow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-3">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title prompt">บันทึกข้อมูลการจัดการเรียนการสอน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body prompt">
                    <p>ต้องการลบข้อมูลการจัดการเรียนการสอน <br>วันจันทร์ที่ 12 มีนาคม 2565 เวลา 08.00 - 10.00 น. </p>
                        
                </div>
                <div class="modal-footer prompt">
                    <button type="button" class="btn btn-success">ตกลง</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function getInsertPro() {
            // Selecting the input element and get its value 
            var insertProf = $("#insertProf").val();
            console.log("innnnn");
            var str = "<b>คุณต้องการเพิ่มอาจารย์ใช่หรือไม่ </b> "
            $('#insertProadd').val(insertProf);
            $('#insertProfessor').modal('show');
            $('#selectNameShow').html(str);
            /// .val send value to controller
            
           

            //can use same
            // document.getElementById('courseID-add-modal').value = courseID;
            // document.getElementById('courseNameTH-add-modal').value = courseNameTH;
            // document.getElementById('courseNameEN-add-modal').value = courseNameEN;
        }
        $('#addFormStartTeach').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var dayTeach = $("#dayTeach").val();
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();
            if ((!!dayTeach) && (!!startTime) && (!!endTime)){
                $('#fire-modal-2').modal('show');
                console.log("modal")
                var str = "<b>ต้องการเพิ่มข้อมูลการจัดการเรียนการสอนใช่หรือไม่</b>"
                $('#detailshow').html(str);
                $('#dayTeach-add').val(dayTeach);
                $('#startTime-add').val(startTime);
                $('#endTime-add').val(endTime);
            }
        });

        function getInsertStu() {
            // Selecting the input element and get its value 
            var insertStu = $("#insertStu").val();
            var str = "<b>คุณต้องการเพิ่มนักศึกษาใช่หรือไม่ </b> "
            $('#insertStuadd').val(insertStu);
            $('#insertStudent').modal('show');
            $('#selectNameStuShow').html(str);
            /// .val send value to controller

            //can use same
            // document.getElementById('courseID-add-modal').value = courseID;
            // document.getElementById('courseNameTH-add-modal').value = courseNameTH;
            // document.getElementById('courseNameEN-add-modal').value = courseNameEN;
        }

        function getInsertStuYear() {
            // Selecting the input element and get its value 
            var insertStuYear = $("#insertStuYear").val();
            var str = "<b>คุณต้องการเพิ่มนักศึกษารหัส "+ insertStuYear +" ใช่หรือไม่ </b> "
            $('#insertStudentYear').modal('show');
            $('#selectNameStuYearShow').html(str);
            /// .val send value to controller
            $('#insertStuYearadd').val(insertStuYear);

            //can use same
            // document.getElementById('courseID-add-modal').value = courseID;
            // document.getElementById('courseNameTH-add-modal').value = courseNameTH;
            // document.getElementById('courseNameEN-add-modal').value = courseNameEN;
        }

    

        xmodal = () => {
            $('#insertProfessor<?php echo $course[0]['courseId'] ?>').modal('hide');
            $('#insertStudent<?php echo $course[0]['courseId'] ?>').modal('hide');
            $('#insertStudentYear<?php echo $course[0]['courseId'] ?>').modal('hide');
            $('#fire-modal-2').modal('hide');
            $('.modal-backdrop').remove();
        }

        x_StuEnroll_modal = (i) => {
            $('#delModalStuEnroll' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
        // x_sub_modal = (i) => {
        //     $('#editModal'+i).modal('hide');
        //     $('#delModal'+i).modal('hide');
        // }
    </script>