<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>ข้อมูลการเข้าชั้นเรียน</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/attendanceList') ?>">ตรวจสอบการเข้าชั้นเรียน</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/showAttendanceList') ?>">ข้อมูลการเข้าชั้นเรียน</a>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card card-primary">
                <div class="card-header prompt">
                    <h4>ข้อมูลรายวิชา</h4>
                </div>
                <div class="card-body prompt">
                    <table class="table table-striped col-12">
                        <tbody>
                            <tr>
                                <th>รหัสวิชา :</th>
                                <td><?php echo $courseOfferedName[0][0]['courseID'] ?></td>
                                <th>อาจารย์ผู้สอน :</th>
                                <td><?php $i = 0;
                                    for ($k = 0; $k < sizeOf($courseOfferedProfessorName[$i]); $k++) {
                                        echo $courseOfferedProfessorPrefixName[$i][$k] . $courseOfferedProfessorName[$i][$k] . " " . $courseOfferedProfessorLastName[$i][$k] . "</br>";
                                    }
                                    $i++ ?></td>
                            </tr>
                            <tr>
                                <th>ชื่อวิชา (ภาษาไทย) :</th>
                                <td><?php echo $courseOfferedName[0][0]['courseNameTH'] ?></td>
                                <th>จำนวนนักศึกษา (คน) :</th>
                                <td><?php $n = 0;
                                    echo count($courseOffered[$n]['studentId']); ?></td>
                            </tr>
                            <tr>
                                <th>ชื่อวิชา (ภาษาอังกฤษ) :</th>
                                <td><?php echo $courseOfferedName[0][0]['courseNameEN'] ?></td>
                                <th>ภาคเรียน/ปีการศึกษา :</th>
                                <td><?php echo $courseOffered[0]['term']; ?>/<?php echo $courseOffered[0]['year']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php
    $role = $this->session->role;
    if ($role == '2') {
    ?>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>ข้อมูลการเข้าชั้นเรียน (รายบุคคล)</h4>
                    </div>
                    <div class="card-body prompt">
                        <table class="table table-striped col-12">
                            <tbody>
                                <tr>
                                    <th>รหัสนักศึกษา :</th>
                                    <td><?php echo $user[0]['studentId'] ?></td>
                                    <th>ชื่อ - นามสกุล</th>
                                    <td><?php echo $user[0]['prefix'] ?><?php echo $user[0]['firstName'] ?> <?php echo $user[0]['lastName'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card card-light">
                                    <div class="card-header">
                                        <h4>จำนวนคาบเรียนทั้งหมด (คาบ)</h4>
                                    </div>
                                    <div class="card-body">
                                        <h1 style="text-align: center;color:#007bff;"><?php echo count($status) ?></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card card-light">
                                    <div class="card-header">
                                        <h4>จำนวนครั้งที่เข้าเรียน (คาบ)</h4>
                                    </div>
                                    <div class="card-body">
                                        <h1 style="text-align: center; color:#28a745;"><?php
                                                                                        $sum = 0;
                                                                                        for ($i = 0; $i < sizeOf($status); $i++) {
                                                                                            if (!empty($studentAttendance[$i])) {
                                                                                                if (in_array($user[0]['studentId'], $studentAttendance[$i])) { ?>
                                            <?php $sum += 1;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        echo $sum;
                                            ?></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card card-light">
                                    <div class="card-header">
                                        <h4>จำนวนครั้งที่ไม่เข้าเรียน (คาบ)</h4>
                                    </div>
                                    <div class="card-body">
                                        <h1 style="text-align: center; color:#fd7e14;"><?php $absen =  count($status) - $sum;
                                                                                        echo $absen;
                                                                                        ?></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-12">
                                <div class="card card-light">
                                    <div class="card-header">
                                        <h4>วันที่ขาดเรียน :</h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        $date = [];
                                        $datein = [];
                                        for ($i = 0; $i < sizeOf($checkClass); $i++) {
                                            if (!empty($studentAttendance[$i])) {
                                                if (in_array($user[0]['studentId'], $studentAttendance[$i])) {
                                                    $datein[] = $checkClass[$i]['date'];
                                                } else {
                                                    $date[] = $checkClass[$i]['date'];
                                                }
                                            } else {
                                                $date[] = $checkClass[$i]['date'];
                                            }
                                        }
                                        echo implode(" , ", $date);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    $role = $this->session->role;
    if ($role != '2') {
    ?>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>ตารางสรุปการเข้าชั้นเรียน</h4>
                        <div class="card-header-action prompt">
                            <button type="button" class="btn btn-success" id="export_button"><i class="fas fa-file-excel"></i> ไฟล์รายงานการเข้าชั้นเรียน</button>
                        </div>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">ลำดับที่</th>
                                        <th style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">รหัสนักศึกษา</th>
                                        <th style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">ชื่อ - นามสกุล</th>
                                        <?php $no = 0;
                                        foreach ($checkClass as $checkClass) : $no++ ?>
                                            <th><?= $no ?></th>
                                        <?php endforeach ?>
                                        <th>รวม</th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <?php
                                    $no = 0;
                                    $period = 0;
                                    foreach ($student as $std) : $no++ ?>
                                        <tr style="font-size: 14px;">
                                            <td><?= $no ?></td>
                                            <td><?= $std[0]['studentId'] ?></td>
                                            <td style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><?php echo $std[0]['prefix'] ?><?php echo $std[0]['firstName'] ?> <?php echo $std[0]['lastName'] ?></td>
                                            <?php
                                            $sum = 0;
                                            for ($i = 0; $i < sizeof($status); $i++) {
                                                if (!empty($studentAttendance[$i])) {
                                                    if (in_array($std[0]['studentId'], $studentAttendance[$i])) { ?>
                                                        <td>1</td>
                                                    <?php $sum += 1;
                                                    } else { ?>
                                                        <td>0</td>
                                                    <?php }
                                                } else { ?>
                                                    <td>0</td>
                                            <?php }
                                            } ?>
                                            <td><?= $sum ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <p><b>หมายเหตุ : </b> 1 = เข้าเรียน , 0 = ไม่เข้าเรียน </p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script>
    function html_table_to_excel(type)
    {
        var data = document.getElementById('sortable-table');
        
        var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

        XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

        XLSX.writeFile(file, '<?=$courseOfferedName[0][0]['courseID']?>.' + type);
    }

    const export_button = document.getElementById('export_button');

    export_button.addEventListener('click', () =>  {
        html_table_to_excel('xlsx');
    });

</script>