<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>ตรวจสอบการเข้าชั้นเรียน</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/attendanceList') ?>">ตรวจสอบการเข้าชั้นเรียน</a>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>แสดงรายการเข้าชั้นเรียน</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th class="col-2">ปีการศึกษา</th>
                                        <th class="col-4">วิชา</th>
                                        <th class="col-2">อาจารย์ผู้สอน</th>
                                        <th class="col-2">จำนวนนักศึกษา (คน)</th>
                                        <th class="col-2"></th>
                                    </tr>
                                </thead>
                                    <tbody class="ui-sortable">
                                        <?php
                                        $no = 1;
                                        $i = 0;
                                        $n = 0;
                                        $j = 0;
                                        $k = 0;
                                        $displayName = [];
                                        foreach ($courseOfferedName as $row) : ?>
                                            <tr style="font-size: 14px;">
                                                <td style="text-align: center;"><?php echo $courseOfferedCount[$i][0]['term']; ?>/<?php echo $courseOfferedCount[$i][0]['year']; ?></td>
                                                <td><b><?php echo $row[0]['courseID'] ?></b><br><?php echo $row[0]['courseNameTH'] ?><br> (<?php echo $row[0]['courseNameEN'] ?>)</td>
                                                <?php if (empty($courseOfferedProfessorName[$i])) {
                                                ?>
                                                <td style="text-align: center"><a class="text-danger">ไม่มีข้อมูล</a></td>
                                                <?php } else { ?>
                                                <td style=""><a class="text"><?php
                                                    for ($j=0; $j < sizeOf($courseOfferedProfessorPrefixName[$i]); $j++) { 
                                                        echo $courseOfferedProfessorPrefixName[$i][$j].$courseOfferedProfessorName[$i][$j]." ".$courseOfferedProfessorLastName[$i][$j]."</br>";
                                                    }
                                                    $i++
                                                ?></a></td>
                                                <?php } ?>
                                                <td style="text-align: center"><a class="text"><?php echo count($courseOfferedCount[$n][0]['studentId']);?></a></td>
                                                <td style="text-align: center;">
                                                    <a href="<?php echo base_url('/ShowAttendanceList/detailAttendance') ?>/<?= $courseOfferedCount[$n][$k]['courseOfferedId']?>"><button class="btn btn-info"><i class="fas fa-folder"></i></button></a>
                                                <?php $n++?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>




