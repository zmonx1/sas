    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-user"></i> อัปโหลดรายชื่อนักศึกษา</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/subjectManagement') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/StudentManagement') ?>">เพิ่มบัญชีผู้ใช้งาน (นักศึกษา)</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/UploadStudent') ?>">อัปโหลดรายชื่อนักศึกษา</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>อัปโหลดรายชื่อนักศึกษา</h4>
                    </div>
                    <form method="POST" action="<?php echo base_url("importStudent/uploadStudent");?>" enctype="multipart/form-data">
                    <div class="card-body prompt">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                            <label>แนบไฟล์</label>
                            <input type="file" class="form-control" name="file">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right prompt">
                        <button class="btn btn-primary" name="upload"><i class="fas fa-file-upload"></i> อัปโหลดไฟล์</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายชื่อนักศึกษา</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr>
                                        <th>รหัสนักศึกษา</th>
                                        <th>ชื่อ-นามสกุล</th>
                                        <th>อีเมล</th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <tr>
                                        <td>61102299</td>
                                        <td>นาย โชติวิชช์ วรเดช</td>
                                        <td>chotiwich.wa@mail.wu.ac.th</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title prompt">อัปโหลดรายชื่อนักศึกษา</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body prompt">
                    <p>ต้องการอัปโหลดไฟล์รายชื่อนักศึกษาหรือไม่</p>
                </div>
                <div class="modal-footer prompt">
                    <button type="button" class="btn btn-success">ตกลง</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div> -->
