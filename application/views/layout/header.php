<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#dropdown-profile" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">

              <!-- <img alt="image" src="<?php echo base_url('assets/img/avatar/avatar-1.png') ?>" class="rounded-circle mr-1"> -->
              <div hidden>
                <span id="firstName"><?php echo $user[0]['email'] ?></span>
                <span id="surName"><?php echo $user[0]['email'] ?></span>
              </div>
              <div id="profileImage"></div>
              <div class="d-sm-none d-lg-inline-block prompt"></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" id=dropdown-profile>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url('/dashboard') ?>" class="dropdown-item prompt">
                <i class="fas fa-user"></i> <?php echo $user[0]['email'] ?>
              </a>
              <a href="<?php echo base_url('auth/logout') ?>" class="dropdown-item has-icon text-danger prompt">
                <i class="fas fa-sign-out-alt"></i> ออกจากระบบ
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand prompt">
            <img alt="image" src="<?php echo base_url('assets/img/banner.png') ?>" width="100%" height="100%">
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">SAS</a>
          </div>
          <ul class="sidebar-menu prompt">
            <?php
            $role = $this->session->role;
            if ($role == '1') {
            ?>
              <li class="menu-header">หน้าหลัก</li>
              <li><a class="nav-link" href="<?php echo base_url('/dashboard') ?>"><i class="fas fa-home"></i> <span>หน้าหลัก</span></a></li>
              <li class="menu-header">จัดการระบบรู้จำใบหน้า</li>
              <li><a class="nav-link" href="<?php echo base_url('/TrainFaceStudent') ?>"><i class="fas fa-chalkboard-teacher"></i> <span>ระบบรู้จำใบหน้า</span></a></li>
              <li class="menu-header">จัดการผู้ใช้งานระบบ</li>
              <li><a class="nav-link" href="<?php echo base_url('/ProfessorManagement') ?>"><i class="fas fa-chalkboard-teacher"></i> <span>อาจารย์</span></a></li>
              <li><a class="nav-link" href="<?php echo base_url('/StudentManagement') ?>"><i class="fas fa-user-graduate"></i> <span>นักศึกษา</span></a></li>
              <li><a class="nav-link" href="<?php echo base_url('/AcademicianManagement') ?>"><i class="fas fa-users"></i> <span>นักวิชาการ</span></a></li>
              <li class="menu-header">จัดการข้อมูลรายวิชา</li>
              <li><a class="nav-link" href="<?php echo base_url('/SubjectManagement') ?>"><i class="fas fa-swatchbook"></i> <span>จัดการข้อมูลรายวิชา</span></a></li>
              <li><a class="nav-link" href="<?php echo base_url('/ScheduleToTeach') ?>"><i class="fas fa-tasks"></i> <span>กำหนดเปิดสอน</span></a></li>
              <li class="menu-header">ตรวจสอบการเข้าชั้นเรียนออนไลน์</li>
              <li><a class="nav-link" href="<?php echo base_url('/UploadCapture') ?>"><i class="fas fa-file-image"></i> <span>บันทึกการเข้าชั้นเรียน</span></a></li>
              <li><a class="nav-link" href="<?php echo base_url('/AttendanceList') ?>"><i class="fas fa-clipboard-check"></i> <span>ตรวจสอบการเข้าชั้นเรียน</span></a></li>
            <?php } else if ($role == '3') {
            ?>
              <li class="menu-header">หน้าหลัก</li>
              <li><a class="nav-link" href="<?php echo base_url('/dashboard') ?>"><i class="fas fa-home"></i> <span>หน้าหลัก</span></a></li>
              <li class="menu-header">ตรวจสอบการเข้าชั้นเรียนออนไลน์</li>
              <li><a class="nav-link" href="<?php echo base_url('/UploadCapture') ?>"><i class="fas fa-file-image"></i> <span>บันทึกการเข้าชั้นเรียน</span></a></li>
              <li><a class="nav-link" href="<?php echo base_url('/AttendanceList') ?>"><i class="fas fa-clipboard-check"></i> <span>ตรวจสอบการเข้าชั้นเรียน</span></a></li>
            <?php } else if ($role == '2') {
            ?>
              <li class="menu-header">หน้าหลัก</li>
              <li><a class="nav-link" href="<?php echo base_url('/dashboard') ?>"><i class="fas fa-home"></i> <span>หน้าหลัก</span></a></li>
              <li class="menu-header">อัปโหลดรูปเพื่อการรู้จำใบหน้า</li>
              <li><a class="nav-link" href="<?php echo base_url('/UploadFaceStudent') ?>"><i class="fas fa-user-circle"></i> <span>อัปโหลดรูปใบหน้า</span></a></li>
              <li class="menu-header">ตรวจสอบการเข้าชั้นเรียนออนไลน์</li>
              <li><a class="nav-link" href="<?php echo base_url('/AttendanceList') ?>"><i class="fas fa-clipboard-check"></i> <span>ตรวจสอบการเข้าชั้นเรียน</span></a></li>
            <?php } ?>
            <li class="menu-header">จัดการข้อมูลส่วนตัว</li>
            <li><a class="nav-link" href="<?php echo base_url('/ProfileAndRePassword') ?>"><i class="far fa-user"></i> <span>ข้อมูลส่วนตัว</span></a></li>
          </ul>
      </div>


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


      <style>
        #profileImage {
          width: 40px;
          height: 40px;
          border-radius: 50%;
          background: #512DA8;
          font-size: 20px;
          color: #fff;
          text-align: center;
          line-height: 42px;
          display: inline-block;
          flex-direction: row;
          margin-left: -10px;
        }
      </style>
      <script>
        $(document).ready(function() {
          var firstName = $('#firstName').text();
          var surName = $('#surName').text().split(".");
          var realSurName
          for (let i = 0; i < surName.length; i++) {
            realSurName = surName[1];
          }
          var ssurname = realSurName
          var intials = $('#firstName').text().charAt(0) + ssurname.charAt(0)
          console.log("surname: " + intials);
          var profileImage = $('#profileImage').text(intials.toUpperCase());
        });
      </script>