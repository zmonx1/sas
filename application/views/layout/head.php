<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>ระบบตรวจสอบการเข้าชั้นเรียนออนไลน์</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/bootstrap/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/fontawesome/css/all.min.css') ?>">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/jqvmap/dist/jqvmap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/summernote/summernote-bs4.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/owlcarousel2/dist/assets/owl.carousel.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/owlcarousel2/dist/assets/owl.theme.default.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/jquery-selectric/selectric.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/dropzonejs/dropzone.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/chocolat/dist/css/chocolat.css') ?>">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/components.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/dropzone.css') ?>">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css?family=PromPT:100,300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/modules/select2/dist/css/select2.css') ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="assets/modules/jquery.min.js"></script>
  <!-- dropzone -->
  <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css' type='text/css' rel='stylesheet'>
  <!--datepicker  -->
  <link href="<?php echo base_url('assets/css/datepicker.css')?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker-thai.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/locales/bootstrap-datepicker.th.js')?>"></script>


  
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  
</head>