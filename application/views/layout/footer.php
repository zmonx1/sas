  <!-- General JS Scripts -->
  <script src="<?php echo base_url('assets/modules/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/popper.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/tooltip.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/nicescroll/jquery.nicescroll.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/moment.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/stisla.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/page/bootstrap-modal.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/chocolat/dist/js/jquery.chocolat.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/page/components-multiple-upload.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/popper.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/tooltip.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/nicescroll/jquery.nicescroll.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/moment.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/stisla.js') ?>"></script>

  <!-- JS Libraies -->
  <script src="<?php echo base_url('assets/modules/jquery.sparkline.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/chart.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/owlcarousel2/dist/owl.carousel.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/summernote/summernote-bs4.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/chocolat/dist/js/jquery.chocolat.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/jquery-selectric/jquery.selectric.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/select2/dist/js/select2.full.min.js') ?>"></script>  
  <script src="<?php echo base_url('assets/js/page/modules-chartjs.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/chart.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/modules/jquery.sparkline.min.js') ?>"></script>

  <!-- Page Specific JS File -->
  <script src="<?php echo base_url('assets/js/page/modules-sparkline.js') ?>"></script>

  <!-- Page Specific JS File -->
  <script src="<?php echo base_url('assets/js/page/index.js') ?>"></script>

  <!-- Template JS File -->
  <script src="<?php echo base_url('assets/js/scripts.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
  <!-- DataTables -->
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
  <!--Pagination DataTables -->
  <!-- datepicker -->
  
  <script>


    $(document).ready(function() {
      $('#sortable-table').DataTable({
        language: {
          "decimal": "",
          "emptyTable": "ไม่มีรายการข้อมูล",
          "info": "แสดงรายการที่ _START_ ถึง _END_ จาก _TOTAL_ รายการ",
          "infoEmpty": "ไม่มีรายการข้อมูล",
          "infoFiltered": "(กรองจากทั้งหมด _MAX_ รายการ)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "แสดง  _MENU_ รายการ",
          "loadingRecords": "กำลังโหลดข้อมูล...",
          "processing": "กำลังประมวลผล...",
          "search": "ค้นหา:",
          "zeroRecords": "ไม่พบรายการที่ค้นหา",
          "paginate": {
            "first": "หน้าแรก",
            "last": "หน้าสุดสุดท้าย",
            "next": "ถัดไป",
            "previous": "ก่อนหน้า"
          },
          "aria": {
            "sortAscending": ": เรียงข้อมูลจากน้อยไปมาก",
            "sortDescending": ": เรียงข้อมูลจากมากไปน้อย"
          }
        }
      });
    });
  </script>
  </body>

  </html>