<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>บันทึกการเข้าชั้นเรียน</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/uploadCapture') ?>">บันทึกการเข้าชั้นเรียน</a>
                </div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <?php
                $no = 1;
                $n = 0; 
                $i = 0;
                $j = 0;
                $k = 0;
                foreach ($courseOfferedName as $row) : ?>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <article class="article">
                            <div class="article-header">
                                <div class="article-image" data-background=<?php echo base_url('assets/img/bg_subject01.jpg') ?> style="background-image: url(&quot;assets/img/bg_subject01.jpg&quot;);">
                                </div>
                                <div class="article-title">
                                    <h2><a><?php echo $row[0]['courseID'] ?><br><?php echo $row[0]['courseNameTH'] ?><br><?php echo $row[0]['courseNameEN'] ?></a></h2>
                                </div>
                            </div>
                            <div class="article-details">
                                <p>ภาคการศึกษา: <?php echo $courseOfferedCount[$i][0]['term']; $i++ ?>/<?php echo $courseOfferedCount[$j][0]['year']; $j++ ?> <br> จำนวนนักศึกษา: <?php echo count($courseOfferedCount[$n][0]['studentId']); $n++ ?> คน </p>
                                <div class="article-cta">
                                    <a href="<?php echo base_url('/checkClassAndSelectSubject/detailCheckClass')?>/<?= $courseOfferedCount[$k][0]['courseOfferedId']; $k++?>" class="btn btn-outline-primary btn-lg btn-icon icon-left">เลือก</a>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</div>