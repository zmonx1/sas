    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4>อัปโหลดรูปการเข้าชั้นเรียน</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/uploadCapture') ?>">บันทึกการเข้าชั้นเรียน</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/checkClassAndSelectSubject') ?>">อัปโหลดรูปการเข้าชั้นเรียน</a>
                    </div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 mb-4">
                        <div class="hero bg-primary text-white">
                            <div class="hero-inner">
                                <h2><?php echo $course[0]['courseID'] ?></h2>
                                <h4 class="lead"><?php echo $course[0]['courseNameTH'] ?> (<?php echo $course[0]['courseNameEN'] ?>)</h4>
                                <div class="mt-4">
                                    <a href="<?php echo base_url('/uploadCapture') ?>" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="fas fa-arrow-circle-left"></i> ย้อนกลับ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header prompt">
                                <h4>บันทึกข้อมูลการเข้าชั้นเรียน</h4>
                                <div class="card-header-action prompt">
                                    <button class="btn btn-primary trigger--fire-modal-1" id="modal-1"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="card-body prompt">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="sortable-table">
                                        <thead>
                                            <tr style="font-size: 14px;" class="text-center">
                                                <th class="col-2">คาบเรียนที่</th>
                                                <th class="col-3">วัน-เดือน-ปี</th>
                                                <th class="col-3">เวลา</th>
                                                <th class="col-1">สถานะ</th>
                                                <th class="col-2"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="ui-sortable">
                                            <?php $no = 1;
                                            foreach ($checkClass as $teach) : ?>
                                                <tr style="font-size: 14px;">
                                                    <td style="text-align: center;"><?php echo $no++ ?></td>
                                                    <td style="text-align: center;"><?php echo $teach['date'] ?></td>
                                                    <td style="text-align: center;"><?php echo $teach['startTime'] ?> - <?php echo $teach['endTime'] ?> น.</td>
                                                    <?php if ($teach['status'] == "1") {
                                                    ?>
                                                        <td style="text-align: center;">
                                                            <div class="badge badge-success">บันทึกแล้ว</div>
                                                        </td>
                                                    <?php } else { ?>
                                                        <td style="text-align: center;">
                                                            <div class="badge badge-danger">ยังไม่บันทึก</div>
                                                        </td>
                                                    <?php } ?>
                                                    <td style="text-align: center;">
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#fire-modal-3<?php echo $teach['checkClassId'] ?>"><i class="fas fa-file-image"></i></button>
                                                        <a href="<?php echo base_url('/checkAttendance/detailCheckAttendance') ?>/<?= $teach['checkClassId'] ?>"><button class="btn btn-warning"><i class="fas fa-edit"></i></button></a>
                                                        <button class="btn btn-danger" data-toggle="modal" data-target="#fire-modal-2<?php echo $teach['checkClassId'] ?>"><i class="fas fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $no = 0;
    foreach ($checkClass as $teach) : $no++ ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-2<?php echo $teach['checkClassId'] ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url("/CheckClassAndSelectSubject/deleteCheckClass") ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบข้อมูลการเข้าชั้นเรียน</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="close_modal('<?php echo $teach['checkClassId'] ?>')">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body prompt">
                            <p>ต้องการลบข้อมูลการเข้าชั้นเรียนหรือไม่</p>
                            <input type="hidden" id="date" name="date" value="<?php echo $teach['date'] ?>">
                            <input type="hidden" id="startTime" name="startTime" value="<?php echo $teach['startTime'] ?>">
                            <input type="hidden" id="endTime" name="endTime" value="<?php echo $teach['endTime'] ?>">
                            <input type="hidden" id="checkClassId" name="checkClassId" value="<?php echo $teach['checkClassId'] ?>">
                            <input type="hidden" id="courseOfferedId" name="courseOfferedId" value="<?php echo $teach['courseOfferedId'] ?>">
                            <input type="hidden" id="status" name="status" value="<?php echo $teach['status'] ?>">
                            <input type="hidden" id="day" name="day" value="<?php echo $teach['day'] ?>">
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="close_modal('<?php echo $teach['checkClassId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <?php $no = 0;
    foreach ($checkClass as $teach) : $no++ ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-3<?php echo $teach['checkClassId'] ?>">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">อัปโหลดรูปการเข้าชั้นเรียน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="close_modal('<?php echo $teach['checkClassId'] ?>')">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <p>ไฟล์ที่รองรับได้แก่ jpg. png. และ jpeg. ขนาดไฟล์ต้องไม่เกิน 20 mb</p>
                        <form method="post" action="<?php echo base_url("/CheckClassAndSelectSubject/uploadFileCapture") ?>/<?= $teach['checkClassId']; ?>"  class="dropzone" id="dropzone<?php echo $teach['checkClassId'] ?>"></form>
                    </div>
                    <div class="modal-footer prompt">
                        <input type="button" class="btn btn-success" id="uploadStd<?php echo $teach['checkClassId'] ?>" value='ตกลง'>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="close_modal('<?php echo $teach['checkClassId'] ?>')">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url("/CheckClassAndSelectSubject/insertCheckClass") ?>/<?= $courseOffered[0]['courseOfferedId']; ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มข้อมูลคาบเรียน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="closeModal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <div class="form-group col-md-12">
                            <label>วัน เดือน ปี</label>
                            <input type="text" class="form-control" name="date" id= "date" data-provide="datepicker" data-date-language="th-th" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>เวลาเริ่มต้น</label>
                            <input type="time" class="form-control" name="startTime" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>เวลาสิ้นสุด</label>
                            <input type="time" class="form-control" name="endTime" required>
                        </div>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js' type='text/javascript'></script>

    <script type="text/javascript">
        
        <?php $no = 0;
        foreach ($checkClass as $teach) : $no++ ?>
            Dropzone.autoDiscover = false;
            var myDropzone<?php echo $teach['checkClassId'] ?> = new Dropzone('#dropzone<?php echo $teach['checkClassId'] ?>', {

                paramName: 'file',
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 20, // Number of files process at a time (default 2)
                addRemoveLinks: true,
                maxFile: 20,
                maxFilesize: 20, // MB
                dictRemoveFile: 'remove',
                dicCancelUpload: 'Cancel',
                dictFileTooBig: "ไม่อนุญาติให้อัพโหลดเกิน 20 MB",
                acceptedFiles: ".jpeg,.jpg,.png",
                init: function() {

                },
                success: function(file, response) {
                    console.log("response");
                    console.log(response);
                    window.location.replace("<?php echo base_url('checkAttendance/setPredict/')?><?php echo $teach['checkClassId'] ?>");

                },
                complete: function(file) {
                    console.log(file)
                }

            });
            $('#uploadStd<?php echo $teach['checkClassId'] ?>').click(function() {
                myDropzone<?php echo $teach['checkClassId'] ?>.processQueue();
            });
        <?php endforeach ?>

        closeModal = () => {
            $('#fire-modal-1').modal('hide');
            $('.modal-backdrop').remove();
            console.log("modal-hide")
        }

        close_modal = (i) => {
            $('#fire-modal-3' + i).modal('hide');
            $('#fire-modal-2' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
        

    </script>
       <script>
       function demo() {
        $('#date').datepicker();
      }
    </script>