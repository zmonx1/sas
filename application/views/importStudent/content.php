<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4><i class="fas fa-file-import"></i> อัปโหลดไฟล์รายชื่อนักศึกษา</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/StudentManagement') ?>">จัดการผู้ใช้งานระบบ (นักศึกษา)</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/ImportStudent') ?>">อัปโหลดไฟล์รายชื่อนักศึกษา</a>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card card-primary">
                <div class="card-header prompt">
                    <h4>อัปโหลดรายชื่อนักศึกษา</h4>
                </div>
                <form method="POST" action="<?php echo base_url("importStudent/uploadStudent"); ?>" enctype="multipart/form-data">
                    <div class="card-body prompt">
                        <div class="form-row">
                            <p>1. ระบบสามารถรองรับไฟล์ นามสกุล <a class="text-danger">.xlsx .xls</a> เท่านั้น <br>2. ไฟล์ที่ทำการอัปโหลดหัวตารางจะต้องประกอบด้วย <a class="text-danger">studentId</a>, <a class="text-danger">prefix</a>, <a class="text-danger">firstName</a>, <a class="text-danger">lastName</a> และ<a class="text-danger"> email </a>ของนักศึกษา <br>3. ระบบจะสร้างรหัสผ่านเริ่มต้นให้แก่นักศึกษา คือ <a class="text-danger">Swe_001</a></p>
                            <!-- <form method="post" id="import_form" enctype="multipart/form-data"> -->
                            <div class="form-group col-md-12">
                                <label>แนบไฟล์</label>
                                <input type="file" name="file" id="file" require accept=".xls, .xlsx" class="form-control">
                            </div>
                            <!-- </form> -->
                        </div>
                    </div>
                    <div class="card-footer text-right prompt">
                        <button type="submit" class="btn btn-primary" name = "upload">ตกลง</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
