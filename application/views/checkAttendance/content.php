    <!-- Main Content -->
    <?php $status= ['เข้าเรียน', 'ไม่เข้าเรียน'] ?>
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4>ข้อมูลการเข้าชั้นเรียน</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/uploadCapture') ?>">บันทึกการเข้าชั้นเรียน</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/checkClassAndSelectSubject') ?>">อัปโหลดรูปการเข้าชั้นเรียน</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/checkAttendance') ?>">ตรวจสอบการเข้าชั้นเรียน</a>
                    </div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header prompt">
                                <h4>ข้อมูลรายละเอียดการเข้าชั้นเรียน</h4>
                            </div>
                            <div style="font-size: 16px;" class="card-body prompt">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label>วิชา</label>
                                        <p><b><?php echo $course[0]['courseID'] ?></b> <br> <?php echo $course[0]['courseNameTH'] ?> <br>(<?php echo $course[0]['courseNameEN'] ?>)</p>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>วัน เดือน ปี</label>
                                        <p><?php echo $checkClass[0]['date'] ?></p>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>เวลา</label>
                                        <p><?php echo $checkClass[0]['startTime'] ?>น. - <?php echo $checkClass[0]['endTime'] ?>น. </p>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>อาจารย์ผู้สอน</label>
                                        <p><?php $i = 0;
                                            for ($k = 0; $k < sizeOf($courseOfferedProfessorName[$i]); $k++) {
                                                echo $courseOfferedProfessorPrefixName[$i][$k] . $courseOfferedProfessorName[$i][$k] . " " . $courseOfferedProfessorLastName[$i][$k] . "</br>";
                                            }
                                            $i++ ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header prompt">
                                <h4>รายชื่อนักศึกษาที่เข้าเรียน</h4>
                            </div>
                            <div class="card-body prompt">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="sortable-table">
                                        <thead>
                                            <tr style="font-size: 14px;" class="text-center">
                                                <th class="col-1">ลำดับที่</th>
                                                <th class="col-2">รหัสนักศึกษา</th>
                                                <th class="col-3">ชื่อ-นามสกุล</th>
                                                <th class="col-2">ค่าความเชื่อมั่น</th>
                                                <th class="col-2">สถานะ</th>
                                                <th class="col-2"></th>
                                            </tr>
                                        </thead>
                                        <?php
                                        $no = 0;
                                        foreach ($student as $std) : $no++; ?>
                                            <tbody class="ui-sortable">
                                                <tr style="font-size: 14px;">
                                                    <td style="text-align: center;"><?= $no ?></td>
                                                    <td style="text-align: center;"><?= $std[0]['studentId'] ?></td>
                                                    <td><?= $std[0]['prefix'] ?><?= $std[0]['firstName'] ?> <?= $std[0]['lastName'] ?></td>
                                                    <td style="text-align: center;"><?= $no ?>%</td>
                                                    <?php if (in_array($std[0]['studentId'], $attendance[0]['studentId'])) { ?>
                                                        <td style="text-align: center;">
                                                            <div class="badge badge-success">เข้าเรียน</div>
                                                        </td>
                                                    <?php } else { ?>
                                                        <td style="text-align: center;">
                                                            <div class="badge badge-danger">ไม่เข้าเรียน</div>
                                                        </td>
                                                    <?php } ?>
                                                    <td style="text-align: center;"><button class="btn btn-warning" data-toggle="modal" data-target="#checkClassModal<?php echo $std[0]['studentId'] ?>"><i class="fas fa-edit"></i></button></td>
                                                </tr>
                                            </tbody>
                                        <?php endforeach ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php $no = 0;
    foreach ($student as $std) : $no++; ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="checkClassModal<?php echo $std[0]['studentId'] ?>">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('checkAttendance/changeStatus'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">แก้ไขการเข้าชั้นเรียน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="x_pro_modal('<?php echo $std[0]['studentId'] ?>')">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <p>ต้องการแก้ไขการเข้าชั้นเรียนของ <?= $std[0]['prefix'] ?><?= $std[0]['firstName'] ?> <?= $std[0]['lastName'] ?></p>
                        <label>สถานะ</label>
                        <input type="hidden" name="checkClass" id="checkClass" value="<?php echo $attendance[0]['checkClassId'] ?>">
                        <input type="hidden" name="stdId" id="stdId" value="<?php echo $std[0]['studentId'] ?>">
                        <select class="form-control" id="status" name="status">
                            <?php for ($i = 0; $i < sizeOf($status); $i++) {
                                $sel = '';
                                if ($status[$i] != in_array($std[0]['studentId'], $attendance[0]['studentId'])) {
                                    $sel = "selected";
                                } ?>
                                <option <?php echo $sel ?>><?php echo $status[$i] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_pro_modal('<?php echo $std[0]['studentId'] ?>')">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php endforeach ?>

    <script>
        x_pro_modal = (i) => {
            $('#checkClassModal' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>