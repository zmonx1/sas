    <!-- Main Content -->
    <?php $PrefixAdmin = ['นาย','นาง','นางสาว'] ?>
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4>จัดการผู้ใช้งานระบบ (นักวิชาการ)</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/academicianManagement') ?>">จัดการผู้ใช้งานระบบ (นักวิชาการ)</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormAcademician" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>เพิ่มบัญชีผู้ใช้งาน (นักวิชาการ)</h4>
                        </div>
                        <div style="font-size: 16px;" class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label>คำนำหน้า*</label>
                                    <select class="form-control" id="adminPrefix" name="adminPrefix" required>
                                        <option value="" disabled selected>เลือกคำนำหน้า</option>
                                        <?php for ($i=0; $i < sizeOf($PrefixAdmin) ; $i++) { ?>
                                            <option><?php echo $PrefixAdmin[$i] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback">กรุณาเลือกคำนำหน้า</div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>ชื่อ*</label>
                                    <input type="text" id="adminFirstName" name="adminFirstName" class="form-control" pattern="^[ก-๏\s]+$" placeholder="ชื่อ" required>
                                    <div class="invalid-feedback">กรุณากรอกชื่อ (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>นามสกุล*</label>
                                    <input type="text" id="adminLastName" name="adminLastName" class="form-control" pattern="^[ก-๏\s]+$" placeholder="นามสกุล" required>
                                    <div class="invalid-feedback">กรุณากรอกนามสกุล (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>อีเมล*</label>
                                    <input type="email" id="adminEmail" name="adminEmail" class="form-control" placeholder="อีเมล" required>
                                    <div class="invalid-feedback">กรุณากรอกอีเมล</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <button class="btn btn-primary" id="addAcademician">เพิ่มนักวิชาการ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายชื่อนักวิชาการ</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th class="col-1">ลำดับที่</th>
                                        <th class="col-4">ชื่อ-นามสกุล</th>
                                        <th class="col-4">อีเมล</th>
                                        <th class="col-3"></th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                <?php $no = 1;
                                foreach ($academician as $admin) : ?>
                                    <tr style="font-size: 14px;">
                                        <td class="text-center"><?php echo $no++ ?></td>
                                        <td><?php echo $admin['prefix'] ?><?php echo $admin['firstName'] ?> <?php echo $admin['lastName'] ?></td>
                                        <td><?php echo $admin['email'] ?></td>
                                        <td style="text-align: center;">
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editAdminModal<?php echo $admin['userId'] ?>"><i class="fas fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delAdminModal<?php echo $admin['userId'] ?>"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertAdminModal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('AcademicianManagement/insertAcademician'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (นักวิชาการ)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="x_Adminmodal()">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" id="adminPrefix-add-modal" name="adminPrefix-add-modal" value="" />
                    <input type="hidden" id="adminFirstName-add-modal" name="adminFirstName-add-modal" value="" />
                    <input type="hidden" id="adminLastName-add-modal" name="adminLastName-add-modal" value="" />
                    <input type="hidden" id="adminEmail-add-modal" name="adminEmail-add-modal" type="text" class="form-control" value="">
                    <!-- <input type="hidden" id="adminPassword-add-modal" name="adminPassword-add-modal" type="text" class="form-control" value="Swe_001">
                    <input type="hidden" id="adminRole-add-modal" name="adminRole-add-modal" type="text" class="form-control" value="2"> -->
                    <div class="modal-body prompt">
                        <p id="adminNameShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_Adminmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php $no = 1;
    foreach ($academician as $admin) : ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editAdminModal<?php echo $admin['userId'] ?>">
            <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('AcademicianManagement/updateAcademician'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">แก้ไขรายชื่อนักวิชาการ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_admin_modal('<?php echo $admin['userId'] ?>')">&times;</span>
                            </button>
                            <input type="hidden" id="adminEmailCheck" name="adminEmailCheck" value="<?php echo $admin['email'] ?>" />
                        </div>
                        <div class="modal-body prompt">
                            <div class="card-body prompt">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>คำนำหน้า:</label>
                                        <select class="form-control" id="PrefixAdmin" name="PrefixAdmin">
                                            <?php for ($i=0; $i < sizeOf($PrefixAdmin) ; $i++) { 
                                                 $sel = '';
                                                 if($PrefixAdmin[$i] == $admin['prefix']){
                                                     $sel = "selected";
                                                 }?>
                                                <option <?php echo $sel ?> ><?php echo $PrefixAdmin[$i] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>ชื่อ:</label>
                                        <input id="firstNameAdmin" name="firstNameAdmin" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $admin['firstName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>นามสกุล:</label>
                                        <input id="lastNameAdmin" name="lastNameAdmin" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $admin['lastName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>อีเมล:</label>
                                        <input id="emailAdmin" name="emailAdmin" type="text" class="form-control" value="<?php echo $admin['email'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_admin_modal('<?php echo $admin['userId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <?php $no = 0;
    foreach ($academician as $admin) : ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="delAdminModal<?php echo $admin['userId'] ?>" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('AcademicianManagement/deleteAcademician'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบนักวิชาการ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_admin_modal('<?php echo $admin['userId'] ?>')">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body prompt">
                            <input type="hidden" id="prefix" name="prefix" type="text" class="form-control" value="<?php echo $admin['prefix'] ?>">
                            <input type="hidden" id="firstName" name="firstName" type="text" class="form-control" value="<?php echo $admin['firstName'] ?>">   
                            <input type="hidden" id="lastName" name="lastName" type="text" class="form-control" value="<?php echo $admin['lastName'] ?>">
                            <input type="hidden" id="email" name="email" type="text" class="form-control" value="<?php echo $admin['email'] ?>">
                            <input type="hidden" id="password" name="password" type="text" class="form-control" value="<?php echo $admin['password'] ?>">
                            <input type="hidden" id="role" name="role" type="text" class="form-control" value="<?php echo $admin['role'] ?>">
                            <input type="hidden" id="userId" name="userId" type="text" class="form-control" value="<?php echo $admin['userId'] ?>">
                            <p>ต้องการลบ <?php echo $admin['prefix'] ?><?php echo $admin['firstName'] ?> <?php echo $admin['lastName'] ?> ออกจากระบบหรือไม่ <br>หากต้องการลบ ระบบจะทำการลบออกจากรายวิชาที่สอน</p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_admin_modal('<?php echo $admin['userId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <script>
        $('#addFormAcademician').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var adminPrefix = $("#adminPrefix").val();
            var adminFirstName = $("#adminFirstName").val();
            var adminLastName = $("#adminLastName").val();
            var adminEmail = $("#adminEmail").val();
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if ((!!adminPrefix) && (!!adminFirstName) && (!!adminLastName) && (!!adminEmail)) {
                if (adminFirstName.match(/^[ก-๛]+$/) && adminLastName.match(/^[ก-๛]+$/) && adminEmail.match(mailformat) ) {
                    $('#insertAdminModal').modal('show');
                    var str = "<br><b>ชื่อ-นามสกุล:</b>  "+ adminPrefix + adminFirstName +' '+ adminLastName + "<br><b>อีเมล:</b> " + adminEmail;
                    $('#adminNameShow').html(str);
                    /// .val send value to controller
                    $('#adminPrefix-add-modal').val(adminPrefix);
                    $('#adminFirstName-add-modal').val(adminFirstName);
                    $('#adminLastName-add-modal').val(adminLastName);
                    $('#adminEmail-add-modal').val(adminEmail);
                }
            }
        });

        x_Adminmodal = () => {
            $('#insertAdminModal').modal('hide');
            $('.modal-backdrop').remove();
            console.log("modal-hide")
        }

        x_admin_modal = (i) => {
            $('#editAdminModal' + i).modal('hide');
            $('#delAdminModal' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>


