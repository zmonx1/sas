    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-swatchbook"></i> จัดการข้อมูลรายวิชา</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/subjectManagement') ?>">จัดการข้อมูลรายวิชา</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormSubject" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>เพิ่มข้อมูลรายวิชา</h4>
                        </div>
                        <div class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label>รหัสวิชา*</label>
                                    <input type="text" class="form-control" id="courseID-add" name="courseID" placeholder="รหัสวิชา" style="text-transform:uppercase" required>
                                    <div class="invalid-feedback">กรุณากรอกรหัสวิชา</div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label>ชื่อวิชา (ภาษาไทย)*</label>
                                    <input type="text" class="form-control" id="courseNameTH-add" name="courseNameTH" pattern="^[ก-๏,0-9,\s]+$" placeholder="ชื่อวิชา (ภาษาไทย)" required>
                                    <div class="invalid-feedback">กรุณากรอกชื่อวิชา (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label>ชื่อวิชา (ภาษาอังกฤษ)*</label>
                                    <input type="text" class="form-control" id="courseNameEN-add" name="courseNameEN" pattern="^[a-zA-Z,0-9,-\s]+$" placeholder="ชื่อวิชา (ภาษาอังกฤษ)" required>
                                    <div class="invalid-feedback">กรุณากรอกชื่อวิชา (ภาษาอังกฤษ)</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <button class="btn btn-primary" id="addSubject">เพิ่มรายวิชา</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>ข้อมูลรายวิชาทั้งหมด</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th class="col-1">ลำดับที่</th>
                                        <th class="col-2">รหัสวิชา</th>
                                        <th class="col-3">ชื่อวิชา (ภาษาไทย)</th>
                                        <th class="col-3">ชื่อวิชา (ภาษาอังกฤษ)</th>
                                        <th class="col-3"></th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <?php $no = 1;
                                    foreach ($course as $cid) : ?>
                                        <tr style="font-size: 14px;">
                                            <td class="text-center"><?php echo $no++ ?></td>
                                            <td class="text-center"><?php echo $cid['courseID'] ?></td>
                                            <td><?php echo $cid['courseNameTH'] ?></td>
                                            <td><?php echo $cid['courseNameEN'] ?></td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editModal<?php echo $cid['courseID'] ?>"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delModal<?php echo $cid['courseID'] ?>"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertModal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('SubjectManagement/insertSubject'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มรายวิชา</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xmodal()">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <input type="hidden" id="courseID-add-modal" name="courseID-add-modal" value="" />
                        <input type="hidden" id="courseNameTH-add-modal" name="courseNameTH-add-modal" value="" />
                        <input type="hidden" id="courseNameEN-add-modal" name="courseNameEN-add-modal" value="" />
                        <input type="hidden" id="coursestatus" name="coursestatus" value="0">
                        <p id="subjectshow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xmodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php $no = 0;
    foreach ($course as $cid) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editModal<?php echo $cid['courseID'] ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('subjectManagement/updateSubject'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">แก้ไขรายวิชา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_sub_modal('<?php echo $cid['courseID'] ?>')">&times;</span>
                            </button>
                        </div>
                        <input type="hidden" id="courseIDOld" name="courseIDOld" value="<?php echo $cid['courseID'] ?>" />
                        <div class="modal-body prompt">
                            <div class="card-body prompt">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>รหัสวิชา:</label>
                                        <input id="courseID" name="courseID" type="text" class="form-control" value="<?php echo $cid['courseID'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>ชื่อวิชา (ภาษาไทย):</label>
                                        <input id="courseNameTH" name="courseNameTH" type="text" class="form-control" pattern="^[ก-๏,0-9,\s]+$" value="<?php echo $cid['courseNameTH'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>ชื่อวิชา (ภาษาอังกฤษ):</label>
                                        <input id="courseNameEN" name="courseNameEN" type="text" class="form-control" pattern="^[a-zA-Z,0-9,-\s]+$" value="<?php echo $cid['courseNameEN'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_sub_modal('<?php echo $cid['courseID'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <?php $no = 0;
    foreach ($course as $cid) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="delModal<?php echo $cid['courseID'] ?>">
            <div class="modal-dialog modal-md">
                <form method="post" action="<?php echo base_url('subjectManagement/deleteSubject'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบรายวิชา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="x_sub_modal('<?php echo $cid['courseID'] ?>')">
                                &times;
                            </button>
                        </div>
                        <input type="hidden" id="cIDOld" name="cIDOld" value="<?php echo $cid['courseID'] ?>" />
                        <div class="modal-body prompt">
                            <input type="hidden" id="courseID" name="courseID" type="text" class="form-control" value="<?php echo $cid['courseID'] ?>">
                            <input type="hidden" id="courseNameTH" name="courseNameTH" type="text" class="form-control" value="<?php echo $cid['courseNameTH'] ?>">
                            <input type="hidden" id="courseNameEN" name="courseNameEN" type="text" class="form-control" value="<?php echo $cid['courseNameEN'] ?>">
                            <p> <b>รหัสวิชา:</b> <?php echo $cid['courseID'] ?> <br> <b>ชื่อวิชา (ภาษาไทย):</b> <?php echo $cid['courseNameTH'] ?> <br> <b>ชื่อวิชา (ภาษาอังกฤษ):</b> <?php echo $cid['courseNameEN'] ?> </p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_sub_modal('<?php echo $cid['courseID'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>
    <script>
        // function getInputValue(e) {
        //     e.preventDefault()
        //     console.log(e)
        //     // Selecting the input element and get its value 
        //     var courseID = $("#courseID-add").val();
        //     var courseNameTH = $("#courseNameTH-add").val();
        //     var courseNameEN = $("#courseNameEN-add").val();
        //     if ((!!courseID) && (!!courseNameTH) && (!!courseNameEN)) {
        //         $('#insertModal').modal('show');
        //         console.log("modal")
        //         var str = "<b>รหัสวิชา:</b> " + courseID + "<br> <b>ชื่อวิชา(ภาษาไทย):</b> " + courseNameTH + "<br> <b>ชื่อวิชา(ภาษาอังกฤษ):</b> " + courseNameEN;
        //         $('#subjectshow').html(str);
        //         $('#courseID-add-modal').val(courseID);
        //         $('#courseNameTH-add-modal').val(courseNameTH);
        //         $('#courseNameEN-add-modal').val(courseNameEN);
        //     }
        // }
        $('#addFormSubject').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var courseID = $("#courseID-add").val();
            var courseNameTH = $("#courseNameTH-add").val();
            var courseNameEN = $("#courseNameEN-add").val();
            if ((!!courseID) && (!!courseNameTH) && (!!courseNameEN)) {
                if (courseID.match(/^[A-Z,0-9,-]+$/) && courseNameTH.match(/^[ก-๛,0-9,' ']+$/) && courseNameEN.match(/^[a-zA-Z,0-9,' ',-]+$/) ) {
                $('#insertModal').modal('show');
                console.log("modal")
                var str = "<b>รหัสวิชา:</b> " + courseID + "<br> <b>ชื่อวิชา(ภาษาไทย):</b> " + courseNameTH + "<br> <b>ชื่อวิชา(ภาษาอังกฤษ):</b> " + courseNameEN;
                $('#subjectshow').html(str);
                $('#courseID-add-modal').val(courseID);
                $('#courseNameTH-add-modal').val(courseNameTH);
                $('#courseNameEN-add-modal').val(courseNameEN);
                }
            }
        });

        xmodal = () => {
            $('#insertModal').modal('hide');
            $('.modal-backdrop').remove();
        }

        x_sub_modal = (i) => {
            $('#editModal' + i).modal('hide');
            $('#delModal' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>