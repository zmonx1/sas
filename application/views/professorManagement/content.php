    <!-- Main Content -->
    <?php $prefixPro = ['ดร.', 'ผศ.', 'ผศ.ดร.', 'รศ.', 'รศ.ดร.', 'ศ.', 'ศ.ดร.', 'อ.'] ?>
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-user"></i> จัดการผู้ใช้งานระบบ (อาจารย์)</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/professorManagement') ?>">จัดการผู้ใช้งานระบบ (อาจารย์)</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormProfessor" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>เพิ่มบัญชีผู้ใช้งาน (อาจารย์)</h4>
                        </div>
                        <div class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label>คำนำหน้า*</label>
                                    <select class="form-control" id="professorPrefix" name="professorPrefix" required>
                                        <option value="" disabled selected>เลือกคำนำหน้า</option>
                                        <?php for ($i = 0; $i < sizeOf($prefixPro); $i++) { ?>
                                            <option><?php echo $prefixPro[$i] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback">กรุณาเลือกคำนำหน้า</div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>ชื่อ*</label>
                                    <input type="text" id="proFirstName" name="proFirstName" class="form-control" placeholder="ชื่อจริง" pattern="^[ก-๏\s]+$" required>
                                    <div class="invalid-feedback">กรุณากรอกชื่อ (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>นามสกุล*</label>
                                    <input type="text" id="proLastName" name="proLastName" class="form-control" placeholder="นามสกุล" pattern="^[ก-๏\s]+$" required>
                                    <div class="invalid-feedback">กรุณากรอกนามสกุล (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>อีเมล*</label>
                                    <input type="email" id="proEmail" name="proEmail" class="form-control" placeholder="อีเมล" required>
                                    <div class="invalid-feedback">กรุณากรอกอีเมล</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <button class="btn btn-primary" id="addProfessor">เพิ่มอาจารย์</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายชื่ออาจารย์ทั้งหมด</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th class="col-2">ลำดับที่</th>
                                        <th class="col-4">ชื่อ - นามสกุล</th>
                                        <th class="col-3">อีเมล</th>
                                        <th class="col-3"></th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <?php $no = 1;
                                    foreach ($professor as $role) : ?>
                                        <tr style="font-size: 14px;">
                                            <td style="text-align: center;"><?php echo $no++ ?></td>
                                            <td><?php echo $role['prefix'] ?><?php echo $role['firstName'] ?> <?php echo $role['lastName'] ?></td>
                                            <td><?php echo $role['email'] ?></td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editProModal<?php echo $role['userId'] ?>"><i class="fas fa-edit"></i></button>
                                                <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editProModal"><i class="fas fa-edit"></i></button> -->
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delModalPro<?php echo $no ?>"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertProModal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('ProfessorManagement/insertProfessor'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (อาจารย์)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xpro_modal()">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" id="proPrefix-add-modal" name="proPrefix-add-modal" value="" />
                    <input type="hidden" id="pFirstName-add-modal" name="pFirstName-add-modal" value="" />
                    <input type="hidden" id="pLastName-add-modal" name="pLastName-add-modal" value="" />
                    <input type="hidden" id="pEmail-add-modal" name="pEmail-add-modal" value="" />
                    <!-- <input type="hidden" id="pPassword-add-modal" name="pPassword-add-modal" class="form-control" value="Swe_001"> -->
                    <!-- <input type="hidden" id="pRole-add-modal" name="pRole-add-modal" class="form-control" value="3"> -->
                    <div class="modal-body prompt">
                        <p id="proNameShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xpro_modal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (อาจารย์)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" onclick="xsuccess_modal()">&times;</span>
                    </button>
                </div>
                <div class="modal-body prompt">
                    <p id="proNameShow2"></p>
                </div>
                <div class="modal-footer prompt">
                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="xsuccess_modal()">ตกลง</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xsuccess_modal()">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>

    <?php $no = 1;
    foreach ($professor as $role) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editProModal<?php echo $role['userId'] ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('ProfessorManagement/updateProfessor'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">แก้ไขรายชื่ออาจารย์</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_pro_modal('<?php echo $role['userId'] ?>')">&times;</span>
                            </button>
                            <input type="hidden" id="proEmailCheck" name="proEmailCheck" value="<?php echo $role['email'] ?>" />
                        </div>
                        <div class="modal-body prompt">
                            <div class="card-body prompt">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>คำนำหน้า:</label>
                                        <select class="form-control" id="prefixPro" name="prefixPro">
                                            <?php for ($i = 0; $i < sizeOf($prefixPro); $i++) {
                                                $sel = '';
                                                if ($prefixPro[$i] == $role['prefix']) {
                                                    $sel = "selected";
                                                } ?>
                                                <option <?php echo $sel ?>><?php echo $prefixPro[$i] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>ชื่อ:</label>
                                        <input id="firstNamePro" name="firstNamePro" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $role['firstName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>นามสกุล:</label>
                                        <input id="lastNamePro" name="lastNamePro" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $role['lastName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>อีเมล:</label>
                                        <input id="emailPro" name="emailPro" type="text" class="form-control" value="<?php echo $role['email'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_pro_modal('<?php echo $role['userId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>



    <?php $no = 1;
    foreach ($professor as $role) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="delModalPro<?php echo $no ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('ProfessorManagement/deleteProfessor'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบอาจารย์</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_pro_modal('<?php echo $no ?>')">&times;</span>
                            </button>
                            <input type="hidden" id="emailOld" name="emailOld" value="<?php echo $role['email'] ?>" />
                        </div>
                        <div class="modal-body prompt">
                            <input type="hidden" id="prefix" name="prefix" type="text" class="form-control" value="<?php echo $role['prefix'] ?>">
                            <input type="hidden" id="firstName" name="firstName" type="text" class="form-control" value="<?php echo $role['firstName'] ?>">
                            <input type="hidden" id="lastName" name="lastName" type="text" class="form-control" value="<?php echo $role['lastName'] ?>">
                            <input type="hidden" id="email" name="email" type="text" class="form-control" value="<?php echo $role['email'] ?>">
                            <input type="hidden" id="password" name="password" type="text" class="form-control" value="<?php echo $role['password'] ?>">
                            <input type="hidden" id="role" name="role" type="text" class="form-control" value="<?php echo $role['role'] ?>">
                            <input type="hidden" id="userId" name="userId" type="text" class="form-control" value="<?php echo $role['userId'] ?>">

                            <p>ต้องการลบ <?php echo $role['prefix'] ?><?php echo $role['firstName'] ?> <?php echo $role['lastName'] ?> ออกจากระบบหรือไม่ <br>หากต้องการลบ ระบบจะทำการลบออกจากรายวิชาที่สอน </p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_pro_modal('<?php echo $no ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <script>
        // function getProValue(e) {
        //     e.preventDefault()
        //     console.log(e)
        //     //Selecting the input element and get its value 
        //     var proPrefix = $("#professorPrefix").val();
        //     var pFirstName = $("#proFirstName").val();
        //     var pLastName = $("#proLastName").val();
        //     var pEmail = $("#proEmail").val();
        //     if ((!!pEmail) && (!!pFirstName) && (!!pLastName) && (!!proPrefix)) {
        //         $('#insertProModal').modal('show');
        //         console.log("modal")
        //         var str = "<br><b>ชื่อ-นามสกุล:</b>  "+ proPrefix + pFirstName +' '+ pLastName + "<br><b>อีเมล:</b> " + pEmail;
        //         $('#proNameShow').html(str);
        //         /// .val send value to controller
        //         $('#proPrefix-add-modal').val(proPrefix);
        //         $('#pFirstName-add-modal').val(pFirstName);
        //         $('#pLastName-add-modal').val(pLastName);
        //         $('#pEmail-add-modal').val(pEmail);
        //     }
        // }
        $('#addFormProfessor').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var proPrefix = $("#professorPrefix").val();
            var pFirstName = $("#proFirstName").val();
            var pLastName = $("#proLastName").val();
            var pEmail = $("#proEmail").val();
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if ((!!pEmail) && (!!pFirstName) && (!!pLastName) && (!!proPrefix)) {
                if (pFirstName.match(/^[ก-๛]+$/) && pLastName.match(/^[ก-๛]+$/) && pEmail.match(mailformat) ) {
                        $('#insertProModal').modal('show');
                        // console.log("modal")
                        var str = "<br><b>ชื่อ-นามสกุล:</b>  " + proPrefix + pFirstName + ' ' + pLastName + "<br><b>อีเมล:</b> " + pEmail;
                        $('#proNameShow').html(str);
                        /// .val send value to controller
                        $('#proPrefix-add-modal').val(proPrefix);
                        $('#pFirstName-add-modal').val(pFirstName);
                        $('#pLastName-add-modal').val(pLastName);
                        $('#pEmail-add-modal').val(pEmail);
                }
            }
        });
        xpro_modal = () => {
            $('#insertProModal').modal('hide');
            $('.modal-backdrop').remove();
            console.log("modal-hide")
        }
        xsuccess_modal = () => {
            $('#insertProModal').modal('hide');
            $('#modalSuccess').modal('hide');
            $('.modal-backdrop').remove();
            console.log("modal-hide")
        }
        x_pro_modal = (i) => {
            $('#delModalPro' + i).modal('hide');
            $('#editProModal' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>