<!-- Main Content -->
<?php 
    header("Access-Control-Allow-Origin:*;");
    header("Access-Control-Allow-Methods: GET, OPTIONS");
?>
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>ระบบรู้จำใบหน้า</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/subjectManagement') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/trainFaceStudent') ?>">ระบบรู้จำใบหน้า</a>
                </div>
            </div>
        </div>
    </section>
    <div class="loader">
        <h5 class="prompt" style="text-align:center" >ระบบกำลังทำการรู้จำใบหน้า</h5>
        <h5 class="prompt" style="text-align:center" >กรุณารอสักครู่...</h5>
        <img class="center" src="<?php echo base_url('assets/img/Curve-Loading.gif') ?>" alt="">
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="success-modal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title prompt">เทรนใบหน้า</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body prompt">
                <p id="message"></p>
            </div>
            <div class="modal-footer prompt">
                <a href="<?php echo base_url('trainFaceStudent') ?>" class="btn btn-success" id="train">ตกลง</a>
            </div>
        </div>
    </div>
</div>

</div>


<style>
    .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
    }
</style>

<script>
    
    function trainStudent() {
        $('.loader').show();
        // $('#messageTrain').html('กำลังประมวลผลกรุณารอสักครู่...'); // Show "Downloading..."
        // Do an ajax request
        $.ajax({
                url: 'http://127.0.0.1:8000/train',
            })
            .done(function(data) { // data what is sent back by the php page
                console.log(data);
                $('#success-modal').modal('show');
                $('#message').html('การรู้จำใบหน้าเสร็จสิ้น')
                $('.loader').hide();

                // display data
            });
    };
    trainStudent();
</script>
