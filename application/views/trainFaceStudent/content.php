<!-- Main Content -->
<div class="main-content">
    <section class="section prompt">
        <div class="section-header">
            <h4>ระบบรู้จำใบหน้า</h4>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/subjectManagement') ?>">หน้าหลัก</a>
                </div>
                <div class="breadcrumb-item active">
                    <a href="<?php echo base_url('/trainFaceStudent') ?>">ระบบรู้จำใบหน้า</a>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card card-primary">
                <div class="card-header prompt">
                    <h4>เทรนใบหน้านักศึกษา</h4>
                </div>
                <div class="card-body prompt">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <br>
                            <label>จำนวนนักศึกษาที่ทำการอัปโหลดรูปใบหน้า</label>
                            <p><?php echo $countStudentUp ?> คน</p>
                        </div>
                        <div class="form-group col-md-3">
                            <br>
                            <label>จำนวนนักศึกษาที่ยังไม่อัปโหลดรูปใบหน้า</label>
                            <p><?php echo $countStudentNotUp ?> คน</p>
                        </div>
                        <div class="form-group col-md-6">
                            <br>
                            <label>นักศึกษาที่ยังไม่อัปโหลดรูปใบหน้า</label>
                            <p><?php echo implode(" , ",$studentIdNotUpload); ?></p>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right prompt"> 
                    <button class="btn btn-primary trigger--fire-modal-1" id="modal-1">เทรนใบหน้า</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title prompt">เทรนใบหน้า</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body prompt">
                <p>ต้องการเทรนใบหน้านักศึกษาหรือไม่</p>
            </div>
            <div class="modal-footer prompt">
                <a href="<?php echo base_url('trainFaceStudent/trainStudent') ?>" class="btn btn-success" id="train">ตกลง</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>
