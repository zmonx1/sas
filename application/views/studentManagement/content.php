    <!-- Main Content -->
    <?php $prefixStu = ['นาย', 'นาง', 'นางสาว'] ?>
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4><i class="fas fa-users"></i> จัดการผู้ใช้งานระบบ (นักศึกษา)</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/StudentManagement') ?>">จัดการผู้ใช้งานระบบ (นักศึกษา)</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <form id="addFormStudent" class="needs-validation" novalidate>
                        <div class="card-header prompt">
                            <h4>เพิ่มบัญชีผู้ใช้งาน (นักศึกษา)</h4>
                            <div class="card-header-action prompt">
                                <a href="<?php echo base_url('/ImportStudent') ?>" class="btn btn-warning"><i class="fas fa-file-import"></i> ไฟล์รายชื่อนักศึกษา</a>
                            </div>
                        </div>
                        <div class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label>รหัสนักศึกษา*</label>
                                    <input type="text" id="stuId" name="stuId" class="form-control" pattern = "[0-9]+$" placeholder="รหัสนักศึกษา" required>
                                    <div class="invalid-feedback">กรุณากรอกรหัสนักศึกษา</div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>คำนำหน้า*</label>
                                    <select class="form-control" id="stuPrefix" name="stuPrefix" required>
                                        <option value="" disabled selected>เลือกคำนำหน้า</option>
                                        <?php for ($i = 0; $i < sizeOf($prefixStu); $i++) { ?>
                                            <option><?php echo $prefixStu[$i] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback">กรุณาเลือกคำนำหน้า</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>ชื่อ*</label>
                                    <input type="text" id="stuFirstName" name="stuFirstName" class="form-control" pattern="^[ก-๏\s]+$" placeholder="ชื่อ" required>
                                    <div class="invalid-feedback">กรุณากรอกชื่อ (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>นามสกุล*</label>
                                    <input type="text" id="stuLastName" name="stuLastName" class="form-control" pattern="^[ก-๏\s]+$" placeholder="นามสกุล" required>
                                    <div class="invalid-feedback">กรุณากรอกนามสกุล (ภาษาไทย)</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>อีเมล*</label>
                                    <input type="email" id="stuEmail" name="stuEmail" class="form-control" placeholder="อีเมล" required>
                                    <div class="invalid-feedback">กรุณากรอกอีเมลนักศึกษา</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right prompt">
                            <button class="btn btn-primary" id="addStudent">เพิ่มนักศึกษา</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header prompt">
                        <h4>รายชื่อนักศึกษาทั้งหมด</h4>
                    </div>
                    <div class="card-body prompt">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                    <tr style="font-size: 14px;" class="text-center">
                                        <th>ลำดับที่</th>
                                        <th>รหัสนักศึกษา</th>
                                        <th>ชื่อ-นามสกุล</th>
                                        <th>อีเมล</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="ui-sortable">
                                    <?php $no = 1;
                                    foreach ($student as $stu) : ?>
                                        <tr style="font-size: 14px;">
                                            <td class="text-center"><?php echo $no++ ?></td>
                                            <td class="text-center"><?php echo $stu['studentId'] ?></td>
                                            <td><?php echo $stu['prefix'] ?><?php echo $stu['firstName'] ?> <?php echo $stu['lastName'] ?></td>
                                            <td><?php echo $stu['email'] ?></td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editModalStu<?php echo $stu['studentId'] ?>"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delModalStu<?php echo $stu['studentId'] ?>"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="insertStuModal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <form method="post" action="<?php echo base_url('StudentManagement/insertStudent'); ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">เพิ่มบัญชีผู้ใช้งาน (นักศึกษา)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onclick="xStumodal()">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" id="stuPrefix-add-modal" name="stuPrefix-add-modal" value="" />
                    <input type="hidden" id="stuId-add-modal" name="stuId-add-modal" value="" />
                    <input type="hidden" id="stuFirstName-add-modal" name="stuFirstName-add-modal" value="" />
                    <input type="hidden" id="stuLastName-add-modal" name="stuLastName-add-modal" value="" />
                    <input type="hidden" id="stuEmail-add-modal" name="stuEmail-add-modal" type="text" class="form-control" value="">
                    <!-- <input type="hidden" id="stuPassword-add-modal" name="stuPassword-add-modal" type="text" class="form-control" value="Swe_001">
                    <input type="hidden" id="stuRole-add-modal" name="stuRole-add-modal" type="text" class="form-control" value="2"> -->
                    <div class="modal-body prompt">
                        <p id="stuNameShow"></p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="xStumodal()">ยกเลิก</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php $no = 1;
    foreach ($student as $stu) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editModalStu<?php echo $stu['studentId'] ?>">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('StudentManagement/updateStudent'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">แก้ไขรายชื่อนักศึกษา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_stu_modal('<?php echo $stu['studentId'] ?>')">&times;</span>
                            </button>
                            <input type="hidden" id="StuIdCheck" name="StuIdCheck" value="<?php echo $stu['studentId'] ?>" />
                        </div>
                        <div class="modal-body prompt">
                            <div class="card-body prompt">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>รหัสนักศึกษา:</label>
                                        <input id="StdId" name="StdId" type="text" class="form-control" pattern = "[0-9]+$" value="<?php echo $stu['studentId'] ?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>คำนำหน้า:</label>
                                        <select class="form-control" id="prefixStu" name="prefixStu">
                                        
                                            <?php for ($i = 0; $i < sizeOf($prefixStu); $i++) { 
                                                $sel = '';
                                                if($prefixStu[$i] == $stu['prefix']){
                                                    $sel = "selected";
                                                }
                                                ?>
                                                <option <?php echo $sel?> ><?php echo $prefixStu[$i] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>ชื่อ:</label>
                                        <input id="firstNameStu" name="firstNameStu" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $stu['firstName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>นามสกุล:</label>
                                        <input id="lastNameStu" name="lastNameStu" type="text" class="form-control" pattern="^[ก-๏\s]+$" value="<?php echo $stu['lastName'] ?>">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>อีเมล:</label>
                                        <input id="emailStu" name="emailStu" type="text" class="form-control" value="<?php echo $stu['email'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_stu_modal('<?php echo $stu['studentId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>

    <?php $no = 0;
    foreach ($student as $stu) : $no++; ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="delModalStu<?php echo $stu['studentId'] ?>" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <form method="post" action="<?php echo base_url('StudentManagement/deleteStudent'); ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title prompt">ลบนักศึกษา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onclick="x_stu_modal('<?php echo $stu['studentId'] ?>')">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body prompt">
                            <input type="hidden" id="prefix" name="prefix" type="text" class="form-control" value="<?php echo $stu['prefix'] ?>">
                            <input type="hidden" id="firstName" name="firstName" type="text" class="form-control" value="<?php echo $stu['firstName'] ?>">
                            <input type="hidden" id="lastName" name="lastName" type="text" class="form-control" value="<?php echo $stu['lastName'] ?>">
                            <input type="hidden" id="email" name="email" type="text" class="form-control" value="<?php echo $stu['email'] ?>">
                            <input type="hidden" id="password" name="password" type="text" class="form-control" value="<?php echo $stu['password'] ?>">
                            <input type="hidden" id="role" name="role" type="text" class="form-control" value="<?php echo $stu['role'] ?>">
                            <input type="hidden" id="userId" name="userId" type="text" class="form-control" value="<?php echo $stu['userId'] ?>">
                            <input type="hidden" id="studentId" name="studentId" type="text" class="form-control" value="<?php echo $stu['studentId'] ?>">
                            <p>ต้องการลบ <?php echo $stu['prefix'] ?><?php echo $stu['firstName'] ?> <?php echo $stu['lastName'] ?> ออกจากระบบหรือไม่ <br>หากต้องการลบ ระบบจะทำการลบออกจากรายวิชาที่เรียน</p>
                        </div>
                        <div class="modal-footer prompt">
                            <button type="submit" class="btn btn-success">ตกลง</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="x_stu_modal('<?php echo $stu['studentId'] ?>')">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endforeach ?>
    <script>
        
        $('#addFormStudent').on('submit', (e) => {
            e.preventDefault()
            console.log(e)
            var stuId = $("#stuId").val();
            var stuPrefix = $("#stuPrefix").val();
            var stuFirstName = $("#stuFirstName").val();
            var stuLastName = $("#stuLastName").val();
            var stuEmail = $("#stuEmail").val();
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if ((!!stuId) && (!!stuPrefix) && (!!stuFirstName) && (!!stuLastName) && (!!stuEmail)) {
                if (stuId.match(/^[0-9]+$/) && stuFirstName.match(/^[ก-๛]+$/) && stuLastName.match(/^[ก-๛]+$/) && stuEmail.match(mailformat) ) {
                    $('#insertStuModal').modal('show');
                    var str = "<b>รหัสนักศึกษา:</b> " + stuId + "<br><b>ชื่อ-นามสกุล:</b>  " + stuPrefix + stuFirstName + ' ' + stuLastName + "<br><b>อีเมล:</b> " + stuEmail;
                    $('#stuNameShow').html(str);
                    /// .val send value to controller
                    $('#stuId-add-modal').val(stuId);
                    $('#stuPrefix-add-modal').val(stuPrefix);
                    $('#stuFirstName-add-modal').val(stuFirstName);
                    $('#stuLastName-add-modal').val(stuLastName);
                    $('#stuEmail-add-modal').val(stuEmail);
                }
            }
        });

        xStumodal = () => {
            $('#insertStuModal').modal('hide');
            $('.modal-backdrop').remove();
            console.log("modal-hide")
        }

        x_stu_modal = (i) => {
            $('#editModalStu' + i).modal('hide');
            $('#delModalStu' + i).modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>