    <!-- Main Content -->
    <div class="main-content">
        <section class="section prompt">
            <div class="section-header">
                <h4>จัดการข้อมูลส่วนตัว</h4>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/dashboard') ?>">หน้าหลัก</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/profileAndRePassword') ?>">ข้อมูลส่วนตัว</a>
                    </div>
                    <div class="breadcrumb-item active">
                        <a href="<?php echo base_url('/rePassword') ?>">แก้ไขรหัสผ่าน</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card card-primary">
                    <form method="POST" action="<?php echo base_url(); ?>repassword/updateNewPassword" id="insert" class="needs-validation" novalidate="">
                        <div class="card-header prompt">
                            <h4>แก้ไขรหัสผ่าน</h4>
                        </div>
                        <div class="card-body prompt">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="password" class="control-label">รหัสผ่าน (เดิม)</label>
                                    <input id="password" type="password" class="form-control" name="password" tabindex="1" id="password" required autofocus>
                                    <div class="invalid-feedback">
                                        โปรดใส่รหัสผ่านเดิมให้ถูกต้อง
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>รหัสผ่าน (ใหม่)</label>
                                    <input id="newPassword" type="password" class="form-control" name="newPassword" id="newPassword" tabindex="2" required autofocus>
                                    <div class="invalid-feedback">
                                        โปรดใส่รหัสผ่านใหม่ให้ตรงกัน
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>ยืนยันรหัสผ่าน</label>
                                    <input id="confirmPassword" type="password" class="form-control" name="confirmPassword" id="confirmPassword" tabindex="3" required>
                                    <div class="invalid-feedback">
                                        โปรดใส่รหัสผ่านใหม่ให้ตรงกัน
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right prompt">
                                <button type="submit" class="btn btn-primary">ยืนยันรหัสผ่าน</button>
                                <a href="<?php echo base_url('/profileAndRePassword') ?>"><button type="button" class="btn btn-secondary">ยกเลิก</button></a>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="modal fade" role="dialog" id="fire-modal-1">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">ยืนยันรหัสผ่าน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <p>ต้องการยืนยันการแก้ไขรหัสผ่านหรือไม่</p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="button" onclick="insert_to()" class="btn btn-primary">ตกลง</button>
                        <button type="button" class="btn btn-secondary" onclick="hide('fire-modal-1')" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-2">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">รหัสผ่านไม่ถูกต้อง</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <p>โปรดกรอกรหัสผ่านใหม่ให้ตรงกัน</p>
                    </div>
                    <div class="modal-footer prompt">
                        <button type="button" class="btn btn-primary" onclick="hide('fire-modal-2')" data-dismiss="modal">ตกลง</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-3">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title prompt">รหัสผ่านไม่ถูกต้อง</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body prompt">
                        <p>โปรดกรอกรหัสผ่านเดิมให้ถูกต้อง</p>
                    </div>
                    <div class="modal-footer prompt">
                        <a href="<?php echo base_url('/rePassword') ?>"></a><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="hide('fire-modal-3')">ตกลง</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script>
            hide = (i) => {
                // alert(i);
                $("#" + i).modal("hide");
            }
            // validation
            $('#insert').on('submit', function(event) {
                event.preventDefault();
                let password = document.getElementById("password").value;
                let newPassword = document.getElementById("newPassword").value;
                let confirmPassword = document.getElementById("confirmPassword").value;
                
                if (newPassword != confirmPassword) {
                    $("#fire-modal-2").modal("show");
                } else if (password != "" && newPassword != "" && confirmPassword != "" && (newPassword == confirmPassword)) {
                    $("#fire-modal-1").modal("show");
                    testModal();
                }
            });

            function testModal() {
                insert_to = () => {
                    event.preventDefault();
                    let password = document.getElementById("password").value;
                    let newPassword = document.getElementById("newPassword").value;
                    let confirmPassword = document.getElementById("confirmPassword").value;
                    $.ajax({
                        type: 'POST',
                        data: {
                            "password": password,
                            "newPassword": newPassword,
                            "confirmPassword": confirmPassword
                        },
                        url: '<?php echo base_url('repassword/updateNewPassword'); ?>',
                        success: (function(data) {
                            console.log(data);
                            if (data == true) {
                                <?php echo base_url('/profileAndRePassword'); ?>
                            }
                        })
                    })
                }
            }
        </script>